/// Common functions for both student & teacher side of the app
$(() => {
    $(".alert-login").show(() => { 
        setTimeout(() => $(".alert-login").fadeOut(2000), 2000);
    });

    const token = $("meta[name='_csrf']").attr("content");
    const header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });

});

function loadQuizzes(userRole) {
    selectedSubject = $("#subject-filter").val()[0];
    localStorage.setItem("selectedSubject", selectedSubject);

    if(!selectedSubject)
        selectedSubject = -1;

    const params = $.param({ 
        'subject-code': selectedSubject,
        'role': userRole
    });

    $("#quizzes").load(`${ctx}load-quizzes?${params}`);
}