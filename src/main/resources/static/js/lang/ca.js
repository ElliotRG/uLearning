const CONFIRM_QUIZ_DELETION =
  "Estàs segur que vols eliminar aquest qüestionari?";
const IMAGE = "Imatge";
const INVALID_UPLOAD = "Mida i format invàlids";
const INVALID_SIZE = "Mida máxima 10MB";