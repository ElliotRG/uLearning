/// Global variables and constants used along the different app pages

// Subject code that identifies the value of a subject option
var selectedSubject = localStorage.getItem("selectedSubject");
const ROLES = { ROLE_TEACHER: "ROLE_TEACHER", ROLE_STUDENT: "ROLE_STUDENT" };