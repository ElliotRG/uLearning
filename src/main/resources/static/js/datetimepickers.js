$(() => {
    /* Tempus Dominus Bootstrap 4 Datetimepicker */
    $.fn.datetimepicker.Constructor.Default = $.extend(
        {},
        $.fn.datetimepicker.Constructor.Default,
        {
            icons: {
                time: "far fa-clock",
                date: "far fa-calendar",
                up: "fas fa-angle-up",
                down: "fas fa-angle-down",
                previous: "fas fa-angle-left",
                next: "fas fa-angle-right",
            },
        }
    )

    // Quiz Expiration Date Picker
    $("#datetimepicker").datetimepicker({
        locale: "ca",
        format: "DD/MM/YYYY HH:mm:ss",
        dayViewHeaderFormat: "MMMM YYYY",
        ignoreReadonly: true,
        allowInputToggle: true,
        focusOnShow: false,
    })

    // Questions Time To Answer Picker
    initTimePicker(".js-timepicker")

    // Hide Tempus Dominus when there's a click outside of it
    $(document).click(function (e) {
        if ($(".bootstrap-datetimepicker-widget").length) {
            if (!document.getElementById("datetimepicker").contains(e.target)) {
                $("#datetimepicker").datetimepicker("hide")
            }

            for (let picker of document.getElementsByClassName("js-timepicker")) {
                if (!picker.contains(e.target)) {
                    $(picker).datetimepicker("hide")
                }
            }
        }
    })
})

function initTimePicker(timePicker) {
    $(timePicker).datetimepicker({
        locale: "ca",
        format: "HH:mm:ss",
        defaultDate: moment("00:00:30", "HH:mm:ss"),
        ignoreReadonly: true,
        allowInputToggle: true,
        focusOnShow: false,
    })
}