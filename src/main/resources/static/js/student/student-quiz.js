var timer;

$(() => {
    // Quiz images zoom
    $(document).on('click', '.quiz__image', (e) => {
        const newUrl = $(e.currentTarget).attr('src') + "#" + new Date().getTime();
        $('.quiz__image--enlarged').attr('src', newUrl);
        $('#quiz-image-modal').modal('show');
    });

    // Student question form submition
    $('#quiz-content').on('click', '#answer-question-button', () => {
        let data = {};
        let answers = [];
        $("form[name='question-form']").serializeArray().forEach((input, i) => {
            if (i > 0) {
                if (input["name"] === "idQuestion") {
                    data["questionId"] = parseInt(input.value)
                } else {
                    answers.push(parseInt(input.value))
                }
            }
        });
        data["answersIds"] = answers

        $.ajax({
            type: "POST",
            url: `${ctx}student/process-question`,
            contentType: "application/json",
            data: JSON.stringify(data), 
            success: (data) => {
                $('#quiz-content').html(data)
                const feedbackContainer = $('.quiz__feedback')
                $('#user-quiz-score').text(feedbackContainer.data('totalScore'))
                if (feedbackContainer.data('quizCompleted')) {
                    $("#quiz-completed-modal").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            }
        });
    });

    // Get next quiz question
    $('#quiz-content').on('click', '#next-question-button', _nextQuestion);
    $("#quiz-timeout-modal").on('hidden.bs.modal', _nextQuestion);

    launchTimer(true)
    
});

function _nextQuestion() {
    $.ajax({
        type: "GET",
        url: `${ctx}student/next-question`,
        success: (data) => {
            $('#quiz-content').html(data)
            launchTimer(false)
        }
    });
}

function launchTimer(firstCall) {
    let timerTag = $("#question-timer");
    let startQuestionDateTime = moment(startDate).utc();
    let timeToAnswer = moment.duration(timerTag.data("timeToAnswer"));
    let endQuizDateTime =  startQuestionDateTime.add(timeToAnswer).utc();

    if(firstCall) {
        _updateTimer(timerTag, endQuizDateTime)
    }

    timer = setInterval(_updateTimer, 1000, timerTag, endQuizDateTime)
}

function _updateTimer(timerTag, endQuizDateTime) {
    let currentDateTime = moment.utc();
    if(endQuizDateTime.diff(currentDateTime) < 60000) {
        timerTag.fadeOut().fadeIn();
        timerTag.addClass("question__timer--timeout");
    }

    if(currentDateTime.isBefore(endQuizDateTime)) {
        let remainingTime = moment(endQuizDateTime.diff(currentDateTime)).utc();
        timerTag.html(remainingTime.format('HH:mm:ss'));
    } else {
        $("#quiz-timeout-modal").modal({
            backdrop: 'static',
            keyboard: false
        });
        console.log(timer)
        clearInterval(timer);
    }
}