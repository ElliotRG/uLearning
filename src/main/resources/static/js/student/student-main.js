$(() => {
    $(document).on('click', '.quiz-box', ((e) => { 
        $(e.currentTarget).find("#answer-quiz")[0].click() 
    }));
    
    $("#subject-filter").change(() => loadQuizzes(ROLES.ROLE_STUDENT));
});