const floatingBtnsContainer = $('.floating-button__btns')

$(() => {
    // Subject Filter
    $("#subject-filter").change(() => loadQuizzes(ROLES.ROLE_TEACHER))

    // Bootstrap Toggle Switch
    $("#switch-quiz-view-mode").bootstrapToggle({
        on: '<i class="fas fa-edit"></i>',
        off: '<i class="fas fa-chart-bar"></i>',
        size: "small",
        onstyle: "info",
        offstyle: "warning"
    })

    $("#switch-quiz-view-mode").change(() => $("#statistics-link").toggle())

    // Floating Buttons
    const extendedClass = 'floating-button__btns--extended'
    const compactedClass = 'floating-button__btns--compacted'

    $("#create-quiz-btn").click(() => {
        if (floatingBtnsContainer.hasClass(extendedClass)) {
            removeAddFloatingButtonsClasses(extendedClass, compactedClass)
        } else {
            removeAddFloatingButtonsClasses(compactedClass, extendedClass)
        }
    })

    $('.floating-button__container').click((e) => {
        switch (e.target.id) {
            case "create-quiz-btn":
            case "create-quiz-btn-icon":
            case "new-quiz-btn":
            case "new-quiz-btn-icon":
            case "import-quiz-btn":
            case "import-quiz-btn-icon":
                // We keep opened the floating buttons if we click on them or its icons,
                // including the trigger button, to let its own onclick event handle 
                // the extended floating buttons.
                break;
            default:
                removeAddFloatingButtonsClasses(extendedClass, compactedClass)
                break;
        }
    })
})

function publishQuiz(idQuiz, publish) {
    $.post(
        `${ctx}teacher/publish-quiz`,
        {
            "idQuiz": idQuiz,
            "publish": publish
        },
        () => loadQuizzes(ROLES.ROLE_TEACHER)
    );
}

function deleteQuiz(idQuiz) {
    if (confirm(CONFIRM_QUIZ_DELETION)) {
        $.ajax({
            type: 'DELETE',
            url: `${ctx}teacher/delete-quiz`,
            data: { "idQuiz": idQuiz },
            success: () => loadQuizzes(ROLES.ROLE_TEACHER)
        });
    }
}

function removeAddFloatingButtonsClasses(rmvCls, addCls) {
    floatingBtnsContainer.removeClass(rmvCls)
    floatingBtnsContainer[0].offsetLeft // Workaround to repaint browser and trigger animation again
    floatingBtnsContainer.addClass(addCls)
}