const acceptedMimeTypes = ["image/png", "image/jpg", "image/jpeg"]

$(() => {
  // Quiz images upload when clicking upload icon
  $(document).on("click", ".js-image-input-prepend-box", (e) => {
    $(e.currentTarget)
      .next(".custom-file")
      .children("input[type='file']")
      .click()
  })

  // Quiz answers correct field
  $(document).on("click", ".input-prepend-box__checkbox", (e) => {
    const checkbox = $(e.currentTarget).find("input[type='checkbox']")
    checkbox.prop("checked", !checkbox.is(":checked"))
    colorAnswerCheckbox(checkbox)
  })

  // Quiz form validation
  $("form[name='quiz-form']").submit(validateForm)

  $(document).on("change", "input[name$='image']", (e) => {
    const image = $(e.currentTarget)[0].files[0]
    if (image) {
      let labelContent = IMAGE
      let maxSizeExceeded = false
      let invalidType = false

      if (image.size > 1024 * 1024) {
        maxSizeExceeded = true
      }

      if (!acceptedMimeTypes.includes(image.type)) {
        invalidType = true
      }

      let errorMsg
      if (maxSizeExceeded && invalidType) {
        errorMsg = INVALID_UPLOAD
      } else if (maxSizeExceeded && !invalidType) {
        errorMsg = INVALID_SIZE
      } else if (!maxSizeExceeded && invalidType) {
        errorMsg = INVALID_FORMAT
      }

      if (errorMsg === undefined) {
        labelContent = image.name
      } else {
        labelContent = `<p class='input-file__error-msg'>${errorMsg}</p>`
      }

      $(e.currentTarget).next(".custom-file-label").html(labelContent)
      $(e.currentTarget).siblings("input[type='hidden']").val(labelContent)
    }
  })

  if ($("#max-size-alert").data("show")) {
    $("#max-size-alert").fadeIn().delay(5000).fadeOut()
  }

  // Add question to the quiz
  $("#add-question").click((e) => {
    e.preventDefault()
    $.post(
      `${ctx}teacher/quiz/add-question`,
      $("form[name='quiz-form']").serialize(),
      (fragment) => {
        $("#questions-fragment").append(fragment)
        $(".js-quiz-question-difficulty:last").selectpicker()
        initTimePicker(".js-timepicker:last")
      }
    )
  })

  // Remove question from the quiz
  $(document).on("click", ".js-remove-question", (e) => {
    const questionToRemove = $(e.currentTarget).data("question")
    $.get(
      `${ctx}teacher/quiz/remove-question`,
      $("form[name='quiz-form']").serialize() +
      `&questionToRemove=${questionToRemove}`,
      (questionsFragment) => {
        $("#questions-fragment").replaceWith(questionsFragment)
        refreshPickers()
        colorCorrectAnswers()
      }
    )
  })

  if (new URLSearchParams(window.location.search).has("edit")) {
    refreshPickers($("#quiz-subject").attr("data"))
    colorCorrectAnswers()
  } else {
    refreshPickers(selectedSubject)
  }
})

function refreshPickers(subjectCode) {
  if (subjectCode !== undefined) {
    const quizSubject = $("#quiz-subject")
    quizSubject.val(subjectCode)
    quizSubject.selectpicker("refresh")
  }

  $(".js-quiz-question-difficulty").each(function () {
    const quizDifficulty = $(this)
    quizDifficulty.val(quizDifficulty.attr("data"))
    quizDifficulty.selectpicker("refresh")
  })
}

function colorCorrectAnswers() {
  $("input[type='checkbox'].input-checkbox").each(function () {
    colorAnswerCheckbox($(this))
  })
}

function colorAnswerCheckbox(checkbox) {
  if (checkbox.is(":checked")) {
    checkbox.parent().addClass("input-prepend-box__checkbox--checked")
  } else {
    checkbox.parent().removeClass("input-prepend-box__checkbox--checked")
  }
}

function validImageSizeAndType(image) {
  if (image) {
    if (image.size > 1024 * 1024 || !acceptedMimeTypes.includes(image.type)) {
      return false
    }
  }

  return true
}

function validateForm() {
  let validForm = true
  $("div[id$='-alert']").fadeOut()

  // Expiration Date validation: Must not be empty
  if (!$("#quiz-expiration-date").val()) {
    $("#no-expiration-date-alert").fadeIn()
    validForm = false
  }

  // Questions validation: At least one question in the quiz
  if ($("#questions-fragment").children().length === 0) {
    $("#no-questions-alert").fadeIn()
    validForm = false
  }

  // Answers validation: At least one correct per question
  $("[id=answers-box]").each((i, answersBox) => {
    if (!$(answersBox).find($("input[type=checkbox]:checked")).length) {
      $("#no-answers-alert").fadeIn()
      validForm = false
    }
  })

  // Images validation: Size and format
  $("input[name$='image']").each((i, input) => {
    if (input.files.length) {
      if (!validImageSizeAndType(input.files[0])) {
        $("#invalid-images-alert").fadeIn()
        validForm = false
      }
    }
  })

  return validForm
}
