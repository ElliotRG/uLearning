const acceptedMimeTypes = ["text/xml", "application/xml", ".xml"]

$(() => {
  $(".custom-file-input").on("change", (e) => {
    const filenames = Array.from($(e.currentTarget)[0].files)
      .map((f) => f.name)
      .join(", ")

    $(e.currentTarget).next(".custom-file-label").html(filenames)
  })

  $("form[name='quiz-import']").submit((event) => {
    event.preventDefault();

    // Expiration Date validation: Must not be empty
    if (!$("#quiz-expiration-date").val()) {
      $("#no-expiration-date-alert").fadeIn().delay(5000).fadeOut()
    } else {
      // File validation: Not empty & valid
      const files = $("#imported-quiz")[0].files
      if (files.length) {
        if (acceptedMimeTypes.includes(files[0].type)) {
          const importForm = $("form[name='quiz-import']")
          const formData = new FormData(document.getElementById("import-form"))
          const submitBtn = $(event.originalEvent.submitter)
          formData.append(submitBtn.attr("name"), submitBtn.val())
          for (var [key, value] of formData.entries()) {
            console.log(key, value);
          }
          $.ajax({
            url: `${ctx}teacher/import-quiz?${importForm.data("csrfParam")}=${importForm.data("csrfToken")}`,
            type: "POST",
            data: formData,
            processData: false, // Prevent jQuery to transform the data object into a query string
            contentType: false, // We let the browser guess the type
            success: (quizId) => {
              if (event.originalEvent.submitter.id === "import-quiz-button") {
                window.location = `/teacher/quiz?edit=${quizId}`
              } else {
                window.location = "/teacher"
              }
            },
            error: (err) => {
              if (err.status == 415 || err.status == 400) {
                $("#invalid-file-alert").fadeIn().delay(5000).fadeOut()
              } else if (err.status == 0) {
                // When there's a MaxUploadSizeException in back end, connection is refused with code 0
                $("#max-size-alert").fadeIn().delay(5000).fadeOut()
              } else {
                $("#import-error-alert").fadeIn().delay(5000).fadeOut()
              }
            }
          })
        } else {
          $("#invalid-file-alert").fadeIn().delay(5000).fadeOut()
        }
      } else {
        $("#no-file-alert").fadeIn().delay(5000).fadeOut()
      }
    }
  })
})