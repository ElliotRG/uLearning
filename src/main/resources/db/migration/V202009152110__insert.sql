/* SUBJECTS */
INSERT INTO ulearning.subject
VALUES (1, 'ATS', 'ATS', 'EI');
INSERT INTO ulearning.subject
VALUES (2, 'GDS', 'GDS', 'EI');
INSERT INTO ulearning.subject
VALUES (3, 'AC', 'AC', 'EI');
INSERT INTO ulearning.subject
VALUES (4, 'SI', 'SI', 'EI');

/* USERS */
INSERT INTO ulearning.user
VALUES (1425097, 'Elliot', 'asdasd');
INSERT INTO ulearning.user
VALUES (1234567, 'Joan', 'asdasd');
INSERT INTO ulearning.user
VALUES (7654321, 'Sergi', 'asdasd');

/* USER_SUBJECT */
INSERT INTO ulearning.user_subject
(`user_niu`, `subject_code`, `user_role`)
VALUES (1425097, 1, 'ROLE_STUDENT');
INSERT INTO ulearning.user_subject
(`user_niu`, `subject_code`, `user_role`)
VALUES (7654321, 1, 'ROLE_TEACHER');
INSERT INTO ulearning.user_subject
(`user_niu`, `subject_code`, `user_role`)
VALUES (7654321, 2, 'ROLE_TEACHER');
INSERT INTO ulearning.user_subject
(`user_niu`, `subject_code`, `user_role`)
VALUES (7654321, 3, 'ROLE_TEACHER');
INSERT INTO ulearning.user_subject
(`user_niu`, `subject_code`, `user_role`)
VALUES (7654321, 4, 'ROLE_TEACHER');
INSERT INTO ulearning.user_subject
(`user_niu`, `subject_code`, `user_role`)
VALUES (1234567, 1, 'ROLE_TEACHER');
INSERT INTO ulearning.user_subject
(`user_niu`, `subject_code`, `user_role`)
VALUES (1234567, 1, 'ROLE_STUDENT');

/* QUIZZES */
INSERT INTO ulearning.quiz
(`subject_code`, `title`, `topic`, `subtopic`, `expiration_date`, `last_modified`, `total_score` ,`enabled`)
VALUES (1, 'QUIZ ATS - Hadoop', 'Big Data', 'Plataformes Big Data', '2020-06-30 18:19:03', '2000-08-05 18:19:03', 30, 1);
INSERT INTO ulearning.quiz
(`subject_code`, `title`, `topic`, `subtopic`, `expiration_date`, `last_modified`, `total_score` ,`enabled`)
VALUES (1, 'QUIZ ATS - Docker', 'DevOps', 'Deployment', '2019-05-14 12:10:43', '2000-08-05 18:19:03', 5, 1);
INSERT INTO ulearning.quiz
(`subject_code`, `title`, `topic`, `subtopic`, `expiration_date`, `last_modified`, `total_score` ,`enabled`)
VALUES (2, 'QUIZ GDS', 'A', 'B', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 10, 1);

/* QUESTIONS */
-- QUIZ ATS --
INSERT INTO ulearning.question
(`quiz_id`, `question`, `image`, `image_path`, `feedback`, `difficulty`, `time_to_answer`)
VALUES (1, '¿Ustedes piensan antes de hablar o hablan tras pensar?', null, null, 'Y cuanto peor para todos mejor', 3, 30);
INSERT INTO ulearning.question
(`quiz_id`, `question`, `image`, `image_path`, `feedback`, `difficulty`, `time_to_answer`)
VALUES (1, '¿Ustedes piensan antes de hablar o hablan tras pensar?', null, null, 'Y cuanto peor para todos mejor', 2, 30);

INSERT INTO ulearning.question
(`quiz_id`, `question`, `image`, `image_path`, `feedback`, `difficulty`, `time_to_answer`)
VALUES (2, 'Pregunta 1 ATS', null, null, 'Feedback pregunta 1 ATS', 1, 30);

-- QUIZ GDS --
INSERT INTO ulearning.question
(`quiz_id`, `question`, `image`, `image_path`, `feedback`, `difficulty`, `time_to_answer`)
VALUES (3, 'Pregunta 1 GDS', null, null, 'Feedback pregunta 1 GDS', 2, 30);

/* ANSWERS */
-- QUIZ ATS --
INSERT INTO ulearning.answer
(`question_id`, `answer`, `image`, `image_path`, `correct`)
VALUES (1, 'Lorem Rajoy Ipsum viva el vino un vaso es un vaso y un plato es un plato.', null, null, 1);
INSERT INTO ulearning.answer
(`question_id`, `answer`, `image`, `image_path`, `correct`)
VALUES (1, 'A veces moverse es bueno, otras veces no mejor para mí el suyo, beneficio político.', null, null, 0);
INSERT INTO ulearning.answer
(`question_id`, `answer`, `image`, `image_path`, `correct`)
VALUES (1, 'Los catalanes hacen cosas es el vecino el que elije al alcalde', null, null, 1);
INSERT INTO ulearning.answer
(`question_id`, `answer`, `image`, `image_path`, `correct`)
VALUES (1, 'Por las carreteras tienen que ir coches y de los aeropuertos tienen que salir aviones', null, null, 0);

INSERT INTO ulearning.answer
(`question_id`, `answer`, `image`, `image_path`, `correct`)
VALUES (2, 'Lorem Rajoy Ipsum viva el vino un vaso es un vaso y un plato es un plato.', null, null, 1);
INSERT INTO ulearning.answer
(`question_id`, `answer`, `image`, `image_path`, `correct`)
VALUES (2, 'A veces moverse es bueno, otras veces no mejor para mí el suyo, beneficio político.', null, null, 0);
INSERT INTO ulearning.answer
(`question_id`, `answer`, `image`, `image_path`, `correct`)
VALUES (2, 'Los catalanes hacen cosas es el vecino el que elije al alcalde', null, null, 1);
INSERT INTO ulearning.answer
(`question_id`, `answer`, `image`, `image_path`, `correct`)
VALUES (2, 'Por las carreteras tienen que ir coches y de los aeropuertos tienen que salir aviones', null, null, 0);

INSERT INTO ulearning.answer
(`question_id`, `answer`, `image`, `image_path`, `correct`)
VALUES (3, 'Lorem Rajoy Ipsum viva el vino un vaso es un vaso y un plato es un plato.', null, null, 1);
INSERT INTO ulearning.answer
(`question_id`, `answer`, `image`, `image_path`, `correct`)
VALUES (3, 'A veces moverse es bueno, otras veces no mejor para mí el suyo, beneficio político.', null, null, 0);
INSERT INTO ulearning.answer
(`question_id`, `answer`, `image`, `image_path`, `correct`)
VALUES (3, 'Los catalanes hacen cosas es el vecino el que elije al alcalde', null, null, 1);
INSERT INTO ulearning.answer
(`question_id`, `answer`, `image`, `image_path`, `correct`)
VALUES (3, 'Por las carreteras tienen que ir coches y de los aeropuertos tienen que salir aviones', null, null, 0);
-- QUIZ GDS --
INSERT INTO ulearning.answer
(`question_id`, `answer`, `image`, `image_path`, `correct`)
VALUES (4, 'Resposta 1 GDS', null, null, 1);
INSERT INTO ulearning.answer
(`question_id`, `answer`, `image`, `image_path`, `correct`)
VALUES (4, 'Resposta 2 GDS', null, null, 0);
INSERT INTO ulearning.answer
(`question_id`, `answer`, `image`, `image_path`, `correct`)
VALUES (4, 'Resposta 3 GDS', null, null, 0);
INSERT INTO ulearning.answer
(`question_id`, `answer`, `image`, `image_path`, `correct`)
VALUES (4, 'Resposta 4 GDS', null, null, 0);
