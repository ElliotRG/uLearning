-- -----------------------------------------------------
-- Table `subject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `subject` (
  `code` INT NOT NULL,
  `long_name` VARCHAR(100) NOT NULL,
  `short_name` VARCHAR(5) NOT NULL,
  `degree` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `quiz`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quiz` (
  `id_quiz` INT NOT NULL AUTO_INCREMENT,
  `subject_code` INT NOT NULL,
  `title` VARCHAR(100) NOT NULL,
  `topic` VARCHAR(50) NULL DEFAULT NULL,
  `subtopic` VARCHAR(50) NULL DEFAULT NULL,
  `expiration_date` DATETIME NOT NULL,
  `last_modified` TIMESTAMP NOT NULL,
  `total_score` INT NOT NULL,
  `enabled` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_quiz`),
  UNIQUE INDEX `id_question_pool_UNIQUE` (`id_quiz` ASC),
  INDEX `fk_question_pool_subject_idx` (`subject_code` ASC),
  CONSTRAINT `fk_quiz_subject`
    FOREIGN KEY (`subject_code`)
    REFERENCES `subject` (`code`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `question` (
  `id_question` INT NOT NULL AUTO_INCREMENT,
  `quiz_id` INT NOT NULL,
  `question` VARCHAR(200) NOT NULL,
  `image` MEDIUMBLOB NULL DEFAULT NULL,
  `image_path` VARCHAR(500) NULL DEFAULT NULL,
  `feedback` VARCHAR(500) NULL DEFAULT NULL,
  `difficulty` VARCHAR(5) NOT NULL,
  `time_to_answer` TIME NOT NULL,
  PRIMARY KEY (`id_question`),
  UNIQUE INDEX `id_question_UNIQUE` (`id_question` ASC),
  INDEX `fk_question_quiz_idx` (`quiz_id` ASC),
  CONSTRAINT `fk_question_quiz`
    FOREIGN KEY (`quiz_id`)
    REFERENCES `quiz` (`id_quiz`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `answer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `answer` (
  `id_answer` INT NOT NULL AUTO_INCREMENT,
  `question_id` INT NOT NULL,
  `answer` VARCHAR(200) NOT NULL,
  `image` MEDIUMBLOB NULL DEFAULT NULL,
  `image_path` VARCHAR(500) NULL DEFAULT NULL,
  `correct` TINYINT NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_answer`),
  UNIQUE INDEX `id_question_UNIQUE` (`id_answer` ASC),
  INDEX `fk_answer_question_idx` (`question_id` ASC),
  CONSTRAINT `fk_answer_question`
    FOREIGN KEY (`question_id`)
    REFERENCES `question` (`id_question`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user` (
  `niu` INT NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `password` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`niu`),
  UNIQUE INDEX `niu_UNIQUE` (`niu` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `user_question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user_question` (
  `id_user_question` INT NOT NULL AUTO_INCREMENT,
  `user_niu` INT NOT NULL,
  `question_id` INT NOT NULL,
  `start_date` DATETIME NOT NULL,
  `correctly_answered` TINYINT NOT NULL,
  PRIMARY KEY (`id_user_question`),
  UNIQUE INDEX `id_user_question_UNIQUE` (`id_user_question` ASC),
  INDEX `fk_user_question_user_idx` (`user_niu` ASC),
  INDEX `fk_user_question_question_idx` (`question_id` ASC),
  CONSTRAINT `fk_user_question_question`
    FOREIGN KEY (`question_id`)
    REFERENCES `question` (`id_question`) ON DELETE CASCADE,
  CONSTRAINT `fk_user_question_user`
    FOREIGN KEY (`user_niu`)
    REFERENCES `user` (`niu`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `user_question_answer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user_question_answer` (
  `id_user_question_answer` INT NOT NULL AUTO_INCREMENT,
  `user_question_id` INT NOT NULL,
  `answer_id` INT NOT NULL,
  PRIMARY KEY (`id_user_question_answer`),
  UNIQUE INDEX `id_user_question_answer_UNIQUE` (`id_user_question_answer` ASC),
  INDEX `fk_user_question_answer_user_question_idx` (`user_question_id` ASC),
  INDEX `fk_user_question_answer_answer_idx` (`answer_id` ASC),
  CONSTRAINT `fk_user_question_answer_answer`
    FOREIGN KEY (`answer_id`)
    REFERENCES `answer` (`id_answer`) ON DELETE CASCADE,
  CONSTRAINT `fk_user_question_answer_user_question`
    FOREIGN KEY (`user_question_id`)
    REFERENCES `user_question` (`id_user_question`) ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `user_quiz`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user_quiz` (
  `id_user_quiz` INT NOT NULL AUTO_INCREMENT,
  `user_niu` INT NOT NULL,
  `quiz_id` INT NOT NULL,
  `score` INT NOT NULL,
  `last_answer_date` DATETIME NULL DEFAULT NULL,
  `completed` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_user_quiz`),
  UNIQUE INDEX `id_user_quiz_UNIQUE` (`id_user_quiz` ASC),
  INDEX `fk_user_quiz_user_idx` (`user_niu` ASC),
  INDEX `fk_user_quiz_quiz_idx` (`quiz_id` ASC),
  CONSTRAINT `fk_user_quiz_quiz`
    FOREIGN KEY (`quiz_id`)
    REFERENCES `quiz` (`id_quiz`),
  CONSTRAINT `fk_user_quiz_user`
    FOREIGN KEY (`user_niu`)
    REFERENCES `user` (`niu`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `user_subject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user_subject` (
  `id_user_subject` INT NOT NULL AUTO_INCREMENT,
  `user_niu` INT NOT NULL,
  `subject_code` INT NOT NULL,
  `user_role` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`id_user_subject`),
  UNIQUE INDEX `id_user_subject_UNIQUE` (`id_user_subject` ASC),
  INDEX `fk_user_subject_user_idx` (`user_niu` ASC),
  INDEX `fk_user_subject_subject_idx` (`subject_code` ASC),
  CONSTRAINT `fk_user_subject_subject`
    FOREIGN KEY (`subject_code`)
    REFERENCES `subject` (`code`),
  CONSTRAINT `fk_user_subject_user`
    FOREIGN KEY (`user_niu`)
    REFERENCES `user` (`niu`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;
