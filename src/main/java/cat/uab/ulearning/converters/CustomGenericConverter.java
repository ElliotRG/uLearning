package cat.uab.ulearning.converters;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.GenericConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import cat.uab.ulearning.model.entities.Subject;
import cat.uab.ulearning.model.services.interfaces.SubjectService;

@Component
public class CustomGenericConverter implements GenericConverter {
	
	@Autowired
	private SubjectService subjectService;

	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		ConvertiblePair[] pairs = new ConvertiblePair[] { 
				new ConvertiblePair(String.class, Subject.class),
				new ConvertiblePair(String.class, Byte[].class),
				new ConvertiblePair(CommonsMultipartFile.class, Byte[].class)};
		
		Set<ConvertiblePair> convertibleTypes = new HashSet<>();
		
		for(ConvertiblePair pair : pairs) {
			convertibleTypes.add(pair);
		}
		
		return convertibleTypes;
	}

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		if (sourceType.getType() == String.class && targetType.getType() == Subject.class) {
			if(source.toString().isEmpty()) {
				return new Subject();
			} else {
				return subjectService.find(Integer.parseInt((String) source));
			}
	    } else if (sourceType.getType() == String.class && targetType.getType() == Byte[].class) {
	    	return null;
	    } else if (sourceType.getType() == CommonsMultipartFile.class && targetType.getType() == Byte[].class) {
	    	byte[] sourceByteArray = ((CommonsMultipartFile) source).getBytes();
	    	if(sourceByteArray.length == 0) {
	    		return null;
	    	}
	    	Byte[] byteArrayObject = new Byte[sourceByteArray.length];
	    	
	    	int i = 0;    
	    	for(byte b: sourceByteArray)
	    		byteArrayObject[i++] = b;
	    	
			return byteArrayObject;
	    } else {
	    	return source;
	    }
	}
	
}
