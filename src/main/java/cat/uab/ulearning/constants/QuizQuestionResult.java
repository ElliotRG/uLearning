package cat.uab.ulearning.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum QuizQuestionResult {

    INCORRECT("incorrect"),
    CORRECT("correct");

    private String result;

    public static String getResultString(boolean isCorrect) {
        return isCorrect ? CORRECT.result : INCORRECT.result;
    }
    
}