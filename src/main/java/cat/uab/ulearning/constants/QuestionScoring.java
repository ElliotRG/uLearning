package cat.uab.ulearning.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@AllArgsConstructor
@Getter
@Log4j2
public enum QuestionScoring {

    LOW(1, 5),
    MID(2, 10),
    HIGH(3, 20);

    private int difficulty;
    private int score;

    public static int getScoreByDifficulty(int difficulty) {
        for (QuestionScoring scoring : QuestionScoring.values()) {
            if (scoring.getDifficulty() == difficulty) {
                return scoring.getScore();
            }
        }
        log.error("Invalid difficulty {}, coulnd't get question score.", difficulty);

        return 0;
    }
    
}