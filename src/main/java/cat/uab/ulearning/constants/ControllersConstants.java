package cat.uab.ulearning.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ControllersConstants {
	
	public static final String REDIRECT_ERROR_PAGE = "redirect:/error/";
	public static final String REDIRECT_ERROR_403 = "redirect:/error/403";
	public static final String REDIRECT_ERROR_404 = "redirect:/error/404";
	public static final String REDIRECT_ERROR_500 = "redirect:/error/500";
	
	public static final String UAB_IP = "158.109.0.0/16";

}
