package cat.uab.ulearning.constants;

import cat.uab.ulearning.model.entities.Answer;
import cat.uab.ulearning.model.entities.Question;
import cat.uab.ulearning.model.entities.Quiz;
import cat.uab.ulearning.model.entities.Subject;
import cat.uab.ulearning.model.entities.User;
import cat.uab.ulearning.model.entities.UserQuestion;
import cat.uab.ulearning.model.entities.UserQuestionAnswer;
import cat.uab.ulearning.model.entities.UserQuiz;
import cat.uab.ulearning.model.entities.UserSubject;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Log4j2
public enum ModelEnum {
	
	USER(User.class.getSimpleName(), User.class),
	SUBJECT(Subject.class.getSimpleName(), Subject.class),
	USER_SUBJECT(UserSubject.class.getSimpleName(), UserSubject.class),
	USER_QUIZ(UserQuiz.class.getSimpleName(), UserQuiz.class),
	USER_QUESTION(UserQuestion.class.getSimpleName(), UserQuestion.class),
	USER_QUESTION_ANSWER(UserQuestionAnswer.class.getSimpleName(), UserQuestionAnswer.class),
	QUIZ(Quiz.class.getSimpleName(), Quiz.class),
	QUESTION(Question.class.getSimpleName(), Question.class),
	ANSWER(Answer.class.getSimpleName(), Answer.class);
	
	private String entityName;
	private Class<?> entityClass;
	
	public static Class<?> find(String entityName) {
	    for (ModelEnum entity : ModelEnum.values()) {
	        if (entity.getEntityName().equalsIgnoreCase(entityName)) {
	            return entity.getClass();
	        }
	    }
	    
	    log.error("No ModelEnum entity name exist for given entity name");
	    return null;
	}
	
	@Override
	public String toString() {
		return this.entityName;
	}

}
