package cat.uab.ulearning.constants;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum RoleEnum {

	STUDENT("ROLE_STUDENT"),
	TEACHER("ROLE_TEACHER"),
	ADMIN("ROLE_ADMIN");
	
	private String role;
	
	public static RoleEnum find(String type) {
	    for (RoleEnum fileType : RoleEnum.values()) {
	        if (fileType.toString().contains(type.toLowerCase())) {
	            return fileType;
	        }
	    }
	    return STUDENT;
	}
	
	@Override
	public String toString() {
		return this.role;
	}
	
	public String getName() {
		return this.toString().replaceAll("ROLE_", "");
	}
}