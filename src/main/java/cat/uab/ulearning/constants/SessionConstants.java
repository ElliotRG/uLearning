package cat.uab.ulearning.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SessionConstants {
	
	public static final String USER_NIU = "userNiu";
	public static final String USER_ROLE = "userRole";
	
}
