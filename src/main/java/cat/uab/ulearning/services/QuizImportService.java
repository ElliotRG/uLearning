package cat.uab.ulearning.services;

import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import cat.uab.ulearning.constants.QuestionScoring;
import cat.uab.ulearning.model.entities.Answer;
import cat.uab.ulearning.model.entities.Question;
import cat.uab.ulearning.model.entities.Quiz;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class QuizImportService {

    public Pair<Boolean, Quiz> importQuiz(MultipartFile quizFile, Quiz importedQuiz) {
        boolean successfulImport = false;
        Document document;
        try {
            document = Jsoup.parse(quizFile.getInputStream(), null, "", Parser.xmlParser());
            Elements quizTags = document.getElementsByTag("quiz");

            if (!quizTags.isEmpty()) {
                for (Element question : quizTags.first().getElementsByAttributeValue("type", "multichoice")) {
                    Elements answers = question.getElementsByTag("answer");
                    if (answers.size() == 4) {
                        Question parsedQuestion = parseQuestion(question);
                        List<Answer> parsedAnswers = parseAnswers(answers, parsedQuestion);
                        parsedQuestion.setAnswers(parsedAnswers);
                        importedQuiz.addQuestion(parsedQuestion);

                        int totalScore = 0;
                        for (Question quizQuestion : importedQuiz.getQuestions()) {
                            totalScore += QuestionScoring.getScoreByDifficulty(quizQuestion.getDifficulty());
                        }
                        importedQuiz.setTotalScore(totalScore);

                        successfulImport = validateImport(importedQuiz);
                    }
                }
            }
        } catch (IOException e) {
            log.catching(e);
        }

        return Pair.of(successfulImport, importedQuiz);
    }

    private Question parseQuestion(Element question) {
        Question parsedQuestion = new Question();

        String questionText = Jsoup.parse(question.getElementsByTag("questiontext").first().text()).text();
        parsedQuestion.setQuestion(questionText);

        String feedback = Jsoup.parse(question.getElementsByTag("incorrectfeedback").first().text()).text();
        parsedQuestion.setFeedback(feedback);

        parsedQuestion.setTimeToAnswer(LocalTime.of(0, 0, 30));
        parsedQuestion.setDifficulty(QuestionScoring.MID.getDifficulty());

        return parsedQuestion;
    }

    private List<Answer> parseAnswers(Elements answers, Question question) {
        List<Answer> parsedAnswers = new ArrayList<>();

        for (Element answer : answers) {
            Answer parsedAnswer = new Answer();

            String answerText = Jsoup.parse(answer.child(0).text()).text();
            parsedAnswer.setAnswer(answerText);
            parsedAnswer.setCorrect(!answer.attr("fraction").equals("0"));
            parsedAnswer.setQuestion(question);

            parsedAnswers.add(parsedAnswer);
        }

        return parsedAnswers;
    }

    private boolean validateImport(Quiz importedQuiz) {
        boolean valid = false;

        if (importedQuiz != null) {
            // Quiz properties validation
            valid = importedQuiz.getSubject() != null && !importedQuiz.getTitle().isEmpty()
                    && importedQuiz.getExpirationDate() != null && importedQuiz.getLastModified() != null
                    && importedQuiz.getTotalScore() != 0
                    && (importedQuiz.getQuestions() != null || !importedQuiz.getQuestions().isEmpty());

            if (valid) {
                for (Question question : importedQuiz.getQuestions()) {
                    if (question == null) {
                        valid = false;
                        break;
                    }

                    // Question properties validation
                    valid = question.getQuiz() != null && !question.getQuestion().isEmpty()
                            && question.getTimeToAnswer() != null && question.getDifficulty() != 0
                            && (question.getAnswers() != null || !question.getAnswers().isEmpty());

                    if (valid) {
                        for (Answer answer : question.getAnswers()) {
                            if (answer == null) {
                                valid = false;
                                break;
                            }

                            // Answer properties validation
                            valid = answer.getQuestion() != null && !answer.getAnswer().isEmpty();
                        }
                    }
                }
            }
        }

        return valid;
    }

}