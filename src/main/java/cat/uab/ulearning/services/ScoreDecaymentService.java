package cat.uab.ulearning.services;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import cat.uab.ulearning.constants.QuestionScoring;
import cat.uab.ulearning.model.entities.Question;
import cat.uab.ulearning.model.entities.UserQuestion;
import cat.uab.ulearning.model.entities.UserQuiz;
import cat.uab.ulearning.model.services.interfaces.UserQuestionService;
import cat.uab.ulearning.model.services.interfaces.UserQuizService;

@Service
public class ScoreDecaymentService {

    private final String DECAYMENT_CRON_RATE = "59 59 23 ? * * *";

    @Autowired
    private UserQuizService userQuizService;

    @Autowired
    private UserQuestionService userQuestionService;

    @Scheduled(cron = DECAYMENT_CRON_RATE)
    private void decayScore() {
        for (UserQuiz userQuiz : userQuizService.getUserQuizzesForDecayment()) {
            System.out.println(userQuiz.getIdUserQuiz() + " " + userQuiz.getLastAnswerDate());

            UserQuestion unansweredUserQuestion = unanswerCandidateUserQuestion(userQuiz.getUser().getNiu(),
                    userQuiz.getQuiz().getIdQuiz());
            if (unansweredUserQuestion != null) {
                updateUserQuiz(userQuiz, unansweredUserQuestion);
            }

        }
    }

    private UserQuestion unanswerCandidateUserQuestion(int userNiu, int quizId) {
        UserQuestion userQuestionToUnanswer = null;

        List<UserQuestion> candidateUserQuestions = userQuestionService.getAnsweredUserQuestionsByUserAndQuiz(userNiu,
                quizId);

        if (!candidateUserQuestions.isEmpty()) {
            userQuestionToUnanswer = candidateUserQuestions.get(new Random().nextInt(candidateUserQuestions.size()));
            userQuestionToUnanswer.setCorrectlyAnswered(false);
            userQuestionService.save(userQuestionToUnanswer);
        }

        return userQuestionToUnanswer;
    }

    private void updateUserQuiz(UserQuiz userQuiz, UserQuestion unansweredUserQuestion) {
        Question unansweredQuestion = unansweredUserQuestion.getQuestion();
        int updatedScore = userQuiz.getScore()
                - QuestionScoring.getScoreByDifficulty(unansweredQuestion.getDifficulty());

        userQuiz.setScore(updatedScore);
        userQuiz.setCompleted(updatedScore >= userQuiz.getQuiz().getTotalScore());

        userQuizService.save(userQuiz);
    }

}