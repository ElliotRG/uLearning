package cat.uab.ulearning.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cat.uab.ulearning.constants.QuestionScoring;
import cat.uab.ulearning.constants.QuizQuestionResult;
import cat.uab.ulearning.constants.RoleEnum;
import cat.uab.ulearning.constants.SessionConstants;
import cat.uab.ulearning.model.dto.QuestionAnswersDTO;
import cat.uab.ulearning.model.entities.Answer;
import cat.uab.ulearning.model.entities.Question;
import cat.uab.ulearning.model.entities.Quiz;
import cat.uab.ulearning.model.entities.User;
import cat.uab.ulearning.model.entities.UserQuestion;
import cat.uab.ulearning.model.entities.UserQuestionAnswer;
import cat.uab.ulearning.model.entities.UserQuiz;
import cat.uab.ulearning.model.services.interfaces.QuestionService;
import cat.uab.ulearning.model.services.interfaces.QuizService;
import cat.uab.ulearning.model.services.interfaces.SubjectService;
import cat.uab.ulearning.model.services.interfaces.UserQuestionAnswerService;
import cat.uab.ulearning.model.services.interfaces.UserQuestionService;
import cat.uab.ulearning.model.services.interfaces.UserQuizService;
import cat.uab.ulearning.model.services.interfaces.UserService;
import cat.uab.ulearning.utils.DateTimeUtils;

/**
 * Student Controller for the student pages and student-related operations.
 * 
 * @author Elliot RG
 *
 */
@Controller
@RequestMapping("/student")
public class StudentController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private SubjectService subjectService;

	@Autowired
	private QuizService quizService;

	@Autowired
	private QuestionService questionService;

	@Autowired
	private UserQuizService userQuizService;

	@Autowired
	private UserQuestionService userQuestionService;

	@Autowired
	private UserQuestionAnswerService userQuestionAnswerService;

	private Quiz quiz;

	@GetMapping
	public String studentPage(Model model, HttpSession session) {
		int studentNiu = (int) session.getAttribute(SessionConstants.USER_NIU);
		session.setAttribute(SessionConstants.USER_ROLE, RoleEnum.STUDENT.toString());
		
		model.addAttribute("subjects", subjectService.getSubjectsByUser(studentNiu));
		model.addAttribute("quizzes",
				quizService.getQuizzesFromAllUserSubjects(studentNiu, RoleEnum.STUDENT.toString()));
		model.addAttribute("userQuizzes", userQuizService.getUserQuizzesIdQuizMap(studentNiu));

		return "main";
	}

	@GetMapping("/quiz")
	public String quizPage(@RequestParam("id") int quizId, Model model, HttpSession session) {
		this.quiz = quizService.find(quizId);

		// Extra security in case someone knows the ID of an expired quiz and tries to access it.
		if (this.quiz.getExpirationDate().before(DateTimeUtils.formatDate(new Date()))) {
			return "redirect:/student";
		}

		int studentNiu = (int) session.getAttribute(SessionConstants.USER_NIU);
		Question question = questionService.fetchNextQuestion(quizId, studentNiu);

		model.addAttribute("quiz", this.quiz);
		model.addAttribute("question", question);
		model.addAttribute("isQuestionPage", true);
		model.addAttribute("userQuizzes", userQuizService.getUserQuizzesIdQuizMap(studentNiu));
		model.addAttribute("startDate", 
		userQuestionService.getOrCreate(userService.find(studentNiu), question).getStartDate());

		return "student/quiz";
	}

	@PostMapping(value = "/process-question", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String processQuestion(@RequestBody QuestionAnswersDTO questionAnswers, Model model, HttpSession session) {
		User user = userService.find((int) session.getAttribute(SessionConstants.USER_NIU));
		UserQuestion userQuestion = saveUserQuestionAndAnswers(user, questionAnswers);
		Pair<Integer, UserQuiz> scorePair = saveOrUpdateUserQuiz(userQuestion);
		UserQuiz userQuiz = scorePair.getSecond();
		
		model.addAttribute("result", QuizQuestionResult.getResultString(userQuestion.isCorrectlyAnswered()));
		model.addAttribute("isQuestionPage", false);
		model.addAttribute("questionScore", scorePair.getFirst());
		model.addAttribute("score", userQuiz.getScore());
		model.addAttribute("completed", userQuiz.isCompleted());
		model.addAttribute("feedback", userQuestion.getQuestion().getFeedback());

		return "student/quizContentWrapper :: quiz-content-wrapper";
	}

	@GetMapping("/next-question")
	public String nextQuestion(Model model, HttpSession session) {
		int studentNiu = (int) session.getAttribute(SessionConstants.USER_NIU);
		Question nextQuestion = questionService.fetchNextQuestion(this.quiz.getIdQuiz(), studentNiu);

		model.addAttribute("question", nextQuestion);
		model.addAttribute("isQuestionPage", true);
		model.addAttribute("startDate", 
			userQuestionService.getOrCreate(userService.find(studentNiu), nextQuestion).getStartDate());

		return "student/quizContentWrapper :: quiz-content-wrapper";
	}

	private UserQuestion saveUserQuestionAndAnswers(User user, QuestionAnswersDTO questionAnswersDTO) {
		List<UserQuestionAnswer> userQuestionAnswersToSave = new LinkedList<>();
		Question question = this.quiz.getQuestionById(questionAnswersDTO.getQuestionId());

		UserQuestion userQuestion = userQuestionService.getOrCreate(user, question);
		
		List<Integer> userQuestionAnswers = questionAnswersDTO.getAnswersIds();
		
		userQuestion.setCorrectlyAnswered(isQuestionCorrectlyAnswered(question, userQuestionAnswers));
		
		for (int questionAnswerId : userQuestionAnswers) {
			UserQuestionAnswer userQuestionAnswer = new UserQuestionAnswer();
			userQuestionAnswer.setAnswer(question.getAnswerById(questionAnswerId));
			userQuestionAnswer.setUserQuestion(userQuestion);
			userQuestionAnswersToSave.add(userQuestionAnswer);
		}

		userQuestionService.save(userQuestion);
		userQuestionAnswerService.saveList(userQuestionAnswersToSave);
		
		return userQuestion;
	}
	
	private Pair<Integer, UserQuiz> saveOrUpdateUserQuiz(UserQuestion userQuestion) {	
		User user = userQuestion.getUser();
		UserQuiz userQuiz = userQuizService.getUserQuizByUserAndQuiz(user.getNiu(), this.quiz.getIdQuiz());
		if (userQuiz == null) {
			userQuiz = new UserQuiz();
			userQuiz.setQuiz(this.quiz);
			userQuiz.setUser(user);
		}

		int questionScore = 0;
		if (userQuestion.isCorrectlyAnswered()) {
			questionScore = QuestionScoring.getScoreByDifficulty(userQuestion.getQuestion().getDifficulty());
		}
		int score = userQuiz.getScore() + questionScore;

		userQuiz.setScore(score);
		// Greater or equal to include those students who have surpassed
		// the total score of the quiz just in the moment when teacher have
		// updated it
		userQuiz.setCompleted(score >= this.quiz.getTotalScore());
		userQuiz.setLastAnswerDate(DateTimeUtils.formatDate(new Date()));
		
		userQuizService.save(userQuiz);

		return Pair.of(questionScore, userQuiz);
	}
	
	private boolean isQuestionCorrectlyAnswered(Question question, List<Integer> userQuestionAnswers) {
		Set<Integer> correctAnswers = new HashSet<>();
		for (Answer answer : question.getAnswers()) {
			if (answer.isCorrect()) {
				correctAnswers.add(answer.getIdAnswer());
			}
		}

		return new HashSet<>(userQuestionAnswers).equals(correctAnswers);
	}
	
	@GetMapping("/quiz/loadImage")
	public ResponseEntity<byte[]> loadImage(@RequestParam("entity") String entityName, @RequestParam("id") int entityId,
			HttpServletResponse response) throws IOException {
		HttpHeaders headers = new HttpHeaders();
		headers.setCacheControl(CacheControl.empty().getHeaderValue());
		headers.setContentType(MediaType.IMAGE_JPEG);
		byte[] imageAsByteArray = quizService.getQuizSubEntityImage(entityName, entityId);
//	    imageAsByteArray = scale(imageAsByteArray, 500, 500);
//	    return new ResponseEntity<byte[]>(imageAsByteArray, headers, HttpStatus.OK);
		return new ResponseEntity<byte[]>(imageAsByteArray, headers, HttpStatus.OK);
	}
	
//	private byte[] scale(byte[] imageAsByteArray, int width, int height) {
//	    try {
//	        BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageAsByteArray));
//	        
//	        if(height == 0) {
//	            height = (width * img.getHeight()) / img.getWidth(); 
//	        }
//	        
//	        if(width == 0) {
//	            width = (height * img.getWidth()) / img.getHeight();
//	        }
//	        
//	        Image scaledImage = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
//	        BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
//	        imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0,0,0), null);
//
//	        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
//	        ImageIO.write(imageBuff, "jpg", buffer);
//
//	        return buffer.toByteArray();
//	    } catch (IOException e) {
//	        log.catching(e);
//	    }
//	    
//	    return null;
//	}

}
