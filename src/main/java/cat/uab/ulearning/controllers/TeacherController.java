package cat.uab.ulearning.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import cat.uab.ulearning.constants.QuestionScoring;
import cat.uab.ulearning.constants.RoleEnum;
import cat.uab.ulearning.constants.SessionConstants;
import cat.uab.ulearning.model.entities.Question;
import cat.uab.ulearning.model.entities.Quiz;
import cat.uab.ulearning.model.services.interfaces.QuestionService;
import cat.uab.ulearning.model.services.interfaces.QuizService;
import cat.uab.ulearning.model.services.interfaces.SubjectService;
import cat.uab.ulearning.model.services.interfaces.UserQuizService;
import cat.uab.ulearning.services.QuizImportService;
import lombok.extern.log4j.Log4j2;

/**
 * Teacher Controller for the teacher pages and teacher-related operations.
 * 
 * @author Elliot RG
 *
 */
@Controller
@RequestMapping("/teacher")
@Log4j2
public class TeacherController {

	@Autowired
	private SubjectService subjectService;

	@Autowired
	private QuizService quizService;

	@Autowired
	private QuestionService questionService;

	@Autowired
	private UserQuizService userQuizService;

	@Autowired
	private QuizImportService quizImportService;

	@GetMapping
	public String teacherPage(Model model, HttpSession session) {
		int teacherNiu = (int) session.getAttribute(SessionConstants.USER_NIU);
		session.setAttribute(SessionConstants.USER_ROLE, RoleEnum.TEACHER.toString());

		model.addAttribute("subjects", subjectService.getSubjectsByUser(teacherNiu));
		model.addAttribute("quizzes",
				quizService.getQuizzesFromAllUserSubjects(teacherNiu, RoleEnum.TEACHER.toString()));

		return "main";
	}

	@GetMapping("/quiz")
	@Transactional
	public String quizCreationPage(@RequestParam(name = "edit", required = false) Integer quizId, Model model,
			HttpSession session) {

		Quiz quiz;
		if (quizId != null) {
			model.addAttribute("edit", true);
			quiz = quizService.find(quizId);
		} else {
			quiz = new Quiz();
			quiz.addQuestion(new Question());
		}
		model.addAttribute("quiz", quiz);

		int teacherNiu = (int) session.getAttribute(SessionConstants.USER_NIU);
		model.addAttribute("subjects", subjectService.getSubjectsByUser(teacherNiu));

		return "teacher/quizCreation";
	}

	@PostMapping("/quiz/add-question")
	public String addQuestion(Quiz quiz, Model model) {
		quiz.addQuestion(new Question());
		model.addAttribute("index", quiz.getQuestions().size() - 1);

		return "teacher/questionForm";
	}

	@GetMapping("/quiz/remove-question")
	public String removeQuestion(Quiz quiz, @RequestParam("questionToRemove") Integer questionToRemove, Model model) {

		Question question = null;
		if (questionToRemove != 0) {
			// If questionToRemove is 0, it may mean that we are in create mode
			// and therefore all questions have ID = 0, so this search would return
			// a false positive as returning always the first question in the quiz with ID =
			// 0.
			// That's the reason of this 'if' condition.
			question = quiz.getQuestionById(questionToRemove);
		}

		if (question == null) {
			// If question is null we may be in create mode or the questionToRemove
			// has just been added in edit mode, so we try searching it by index
			// since it has not been stored in DB yet and therefore it has not got any ID
			// either.
			question = quiz.getQuestions().get(questionToRemove);
		}

		// If we have finally found the question to remove after searching it,
		// we remove it from the quiz.
		if (question != null) {
			quiz.removeQuestion(question);
			model.addAttribute("index", quiz.getQuestions().size() - 1);
		}

		model.addAttribute("edit", true);

		return "teacher/quizCreation :: #questions-fragment";
	}

	@PostMapping("/quiz/save")
	public String saveQuiz(Quiz quiz,
			@RequestParam(name = "revoke-completion", required = false) boolean revokeCompletion) {
		int totalScore = 0;
		for (Question question : quiz.getQuestions()) {
			totalScore += QuestionScoring.getScoreByDifficulty(question.getDifficulty());
			question.setQuiz(quiz);
		}
		quiz.setTotalScore(totalScore);

		Quiz existingQuiz = quizService.find(quiz.getIdQuiz());

		if (existingQuiz != null) {
			// If quiz exists it may have been edited so we remove from the existing quiz
			// question list the coincident ones in the received quiz.
			List<Question> oldQuestions = existingQuiz.getQuestions();
			oldQuestions.removeAll(quiz.getQuestions());

			// Now we remove from DB the remainig ones, which are the ones that have
			// been deleted in the edited quiz.
			for (Question questionToRemove : oldQuestions) {
				questionService.delete(questionToRemove);
			}

			userQuizService.updateUserQuizzesCompletion(existingQuiz.getIdQuiz(), totalScore, revokeCompletion);
		}

		quizService.save(quiz);

		return "redirect:/teacher";
	}

	@GetMapping("/import-quiz")
	public String importQuizPage(HttpSession session, Model model) {
		int teacherNiu = (int) session.getAttribute(SessionConstants.USER_NIU);
		model.addAttribute("subjects", subjectService.getSubjectsByUser(teacherNiu));

		return "teacher/quizImport";
	}

	@PostMapping("/import-quiz")
	public ResponseEntity<?> importQuiz(Quiz quizImport, @RequestParam("quiz") MultipartFile quizFile) {
		ResponseEntity<?> response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		if (!quizFile.isEmpty()) {
			String mimeType;
			try {
				mimeType = new Tika().detect(quizFile.getBytes());

				if (mimeType.equals(MimeTypeUtils.TEXT_XML_VALUE)
						|| mimeType.equals(MimeTypeUtils.APPLICATION_XML_VALUE)) {
					Pair<Boolean, Quiz> importResult = quizImportService.importQuiz(quizFile, quizImport);
					if (importResult.getFirst()) {
						Quiz importedQuiz = importResult.getSecond();
						quizService.save(importedQuiz);
						response = ResponseEntity.ok(importedQuiz.getIdQuiz());
					} else {
						response = ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
					}
				} else {
					response = ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).build();
				}
			} catch (IOException e) {
				log.catching(e);
			}
		} else {
			response = ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}

		return response;
	}

	@PostMapping("/publish-quiz")
	public ResponseEntity<HttpStatus> publishQuiz(@RequestParam("idQuiz") int idQuiz,
			@RequestParam("publish") boolean publish) {
		quizService.updateQuizPublication(idQuiz, publish);
		return ResponseEntity.ok().build();
	}

	@DeleteMapping("/delete-quiz")
	public ResponseEntity<HttpStatus> deleteQuiz(@RequestParam("idQuiz") int idQuiz) {
		quizService.delete(quizService.find(idQuiz));

		return ResponseEntity.accepted().build();
	}

	@GetMapping("/statistics")
	public String statisticsPage() {
		return "teacher/statistics";
	}

}
