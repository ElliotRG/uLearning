package cat.uab.ulearning.controllers.exceptions;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import cat.uab.ulearning.constants.ControllersConstants;
import lombok.extern.log4j.Log4j2;

@ControllerAdvice
@Log4j2
public class GlobalDefaultExceptionHandler {

	@ExceptionHandler(Exception.class)
	public String defaultErrorHandler(Exception exception) throws Exception {
		if (AnnotationUtils.findAnnotation(exception.getClass(), ResponseStatus.class) != null) {
			throw exception;
		}

		log.catching(exception);
		return ControllersConstants.REDIRECT_ERROR_500;
	}

	@ExceptionHandler(MaxUploadSizeExceededException.class)
	public String handleMaxSizeException(HttpServletRequest request, Model model,
			MaxUploadSizeExceededException exception) {
		model.addAttribute("fileMaxSizeExceeded", true);
		log.catching(exception);
		String redirectPage;
		if (request.getServletPath().contains("import")) {
			redirectPage = "/teacher/quizImport";
		} else {
			redirectPage = "/teacher/quizCreation";
		}

		return redirectPage;
	}

}
