package cat.uab.ulearning.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cat.uab.ulearning.configuration.security.authentication.CustomUserDetails;
import cat.uab.ulearning.configuration.security.authentication.Redirecter;
import cat.uab.ulearning.constants.RoleEnum;
import cat.uab.ulearning.constants.SessionConstants;
import cat.uab.ulearning.model.services.interfaces.QuizService;
import cat.uab.ulearning.model.services.interfaces.UserQuizService;
import lombok.extern.log4j.Log4j2;

/**
 * Main Controller for the main application pages and common operations for all
 * users.
 * 
 * @author Elliot RG
 *
 */
@Controller
@RequestMapping("/")
@Log4j2
public class MainController {

	private static final String CREDENTIALS_ERROR = "error";
	private static final String LOGOUT_SUCCESSFUL = "logout";
	private static final String REDIRECT_LOGIN = "redirect:/login";

	@Autowired
	private QuizService quizService;

	@Autowired
	private UserQuizService userQuizService;

	@GetMapping
	public String loadByDefault() {
		return REDIRECT_LOGIN;
	}

	@GetMapping("/login")
	public String login(@RequestParam(value = CREDENTIALS_ERROR, required = false) String credentialsError,
			@RequestParam(value = LOGOUT_SUCCESSFUL, required = false) String logout,
			@AuthenticationPrincipal CustomUserDetails loggedUser) {

		if (loggedUser != null) {
			// If the user is already logged and goes straight
			// to /login, it's automatically redirected
			if (logout == null && credentialsError == null && !loggedUser.getUsername().equals("anonymousUser")) {
				return "redirect:" + Redirecter.determineTargetUrl(loggedUser);
			}
		}

		return "login";
	}

	@GetMapping("/index")
	public String indexPage() {
		return "index";
	}

	@GetMapping("/load-quizzes")
	public String loadQuizzes(@RequestParam("subject-code") int subjectCode, @RequestParam("role") String role,
			@AuthenticationPrincipal CustomUserDetails loggedUser, HttpSession session, Model model) {
		if (loggedUser.getAuthorities().contains(new SimpleGrantedAuthority(role))) {
			int userNiu = (int) session.getAttribute(SessionConstants.USER_NIU);
			if (subjectCode == -1) {
				// No subject filter selected, load all quizzes from all user subjects
				model.addAttribute("quizzes", quizService.getQuizzesFromAllUserSubjects(userNiu, role));
			} else {
				model.addAttribute("quizzes", quizService.getQuizzesBySubject(subjectCode, role));
			}

			if (RoleEnum.STUDENT.toString().equals(role)) {
				model.addAttribute("userQuizzes", userQuizService.getUserQuizzesIdQuizMap(userNiu));
			}

			return "main :: quizzes";
		} else {
			log.error("Given role is not one of the logged user roles");
		}

		return "main";
	}

}