package cat.uab.ulearning.controllers;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import cat.uab.ulearning.constants.ControllersConstants;

@Controller
@RequestMapping("/error")
public class CustomErrorController implements ErrorController {

	@RequestMapping
	public String handleError(HttpServletRequest request, Model model) {
		Integer statusCode = (Integer) request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

		if (statusCode != null) {
			return ControllersConstants.REDIRECT_ERROR_PAGE + statusCode;
		}

		Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
		String exceptionMessage = getExceptionMessage(throwable, statusCode);
		model.addAttribute("errorMessage", exceptionMessage);

		return "error/error";
	}

	private String getExceptionMessage(Throwable throwable, Integer statusCode) {
		if (throwable != null) {
			return ExceptionUtils.getRootCauseMessage(throwable);
		}
		HttpStatus httpStatus = HttpStatus.valueOf(statusCode);
		return httpStatus.getReasonPhrase();
	}

	@GetMapping("/403")
	public String accessDeniedErrorPage() {
		return "error/403";
	}

	@GetMapping("/404")
	public String notFoundErrorPage() {
		return "error/404";
	}

	@GetMapping("/500")
	public String internalServerErrorPage() {
		return "error/500";
	}

	@Override
	public String getErrorPath() {
		return "/error";
	}

}
