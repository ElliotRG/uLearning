package cat.uab.ulearning.configuration.security.authentication;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import cat.uab.ulearning.constants.RoleEnum;

public class Redirecter {

    public static String determineTargetUrl(CustomUserDetails loggedUser) {
        Collection<? extends GrantedAuthority> authorities = loggedUser.getAuthorities();
        String targetUrl = "";
        
        if (authorities.contains(new SimpleGrantedAuthority(RoleEnum.ADMIN.toString()))) {
        	targetUrl = "/admin";
        } else {
        	if (authorities.stream().anyMatch(authority -> 
        	authority.getAuthority().equalsIgnoreCase(RoleEnum.TEACHER.toString()))) {
        		targetUrl = "/teacher";
        	}
        	
        	if (authorities.stream().anyMatch(authority -> 
        	authority.getAuthority().equalsIgnoreCase(RoleEnum.STUDENT.toString()))) {
        		targetUrl = targetUrl.isEmpty() ? "/student" : "/index";         			
        	}
        }
        
        return targetUrl;
    }
    
}
