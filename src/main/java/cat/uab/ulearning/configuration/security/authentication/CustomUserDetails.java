package cat.uab.ulearning.configuration.security.authentication;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import lombok.Getter;


public class CustomUserDetails extends User {
	
	private static final long serialVersionUID = 3284212911907771646L;
	
	@Getter
	private int niu;

	public CustomUserDetails(int niu, String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, true, true, true, true, authorities);
		this.niu = niu;
	}

}
