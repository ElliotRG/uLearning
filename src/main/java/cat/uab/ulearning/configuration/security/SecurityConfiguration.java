package cat.uab.ulearning.configuration.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import cat.uab.ulearning.configuration.security.authentication.CustomAuthenticationSuccessHandler;
import cat.uab.ulearning.configuration.security.authentication.CustomPasswordEncoder;
import cat.uab.ulearning.configuration.security.authentication.CustomUserDetailsService;
import cat.uab.ulearning.constants.RoleEnum;

/**
 * Configuration class for Spring Security
 * 
 * @author Elliot RG
 *
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private CustomUserDetailsService userDetailsService;
    
    @Autowired
    private CustomAccessDeniedHandler customAccessDeniedHandler;
    
    @Autowired
    private CustomAuthenticationSuccessHandler customAuthSuccessHandler;
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Pages that not require to be logged
        http.authorizeRequests().antMatchers("/", "/login").permitAll()
 
        // Pages that do require to be logged
        // If no login, it will redirect to /login page.
        
        // Student pages require login as ROLE_STUDENT
        .antMatchers("/student/**").hasRole(RoleEnum.STUDENT.getName())
        // Teacher pages require login as ROLE_TEACHER
        .antMatchers("/teacher/**").hasRole(RoleEnum.TEACHER.getName())
        .anyRequest().permitAll()
        // Configuration for login process
    	.and().formLogin()
			.loginProcessingUrl("/login")
			.loginPage("/login")
			.failureUrl("/login?error")
			.usernameParameter("niu")
			.passwordParameter("password")
			.successHandler(customAuthSuccessHandler)
			.permitAll()
        // Configuration for logout process
		.and().logout()
			.logoutUrl("/logout")
			.logoutSuccessUrl("/login?logout")
            .invalidateHttpSession(true)
            .clearAuthentication(true)
            .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
            .permitAll()
        // When user tries to access a page not allowed for his role AccessDeniedException will be thrown.
        .and().exceptionHandling()
            .accessDeniedHandler(customAccessDeniedHandler)
        .and().csrf()
            .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
        .and().headers()
            .xssProtection();
    }
    
    @Override
    @Autowired
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
    	// Setting Service to find User in the database.
    	auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
        auth.jdbcAuthentication().dataSource(dataSource);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
        	.antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**");
    }
    
    @Bean
    public PasswordEncoder passwordEncoder() {
    	return new CustomPasswordEncoder();
    }

}