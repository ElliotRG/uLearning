package cat.uab.ulearning.configuration.security.authentication;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import cat.uab.ulearning.constants.RoleEnum;
import cat.uab.ulearning.constants.SessionConstants;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * Custom implementation of AuthenticationSuccessHandler to manage
 * the URL redirection when login process is successful
 * 
 * @author Elliot RG
 *
 */
@Component
@Log4j2
@Getter
@Setter
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
	 
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
 
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, 
      HttpServletResponse response, Authentication authentication) throws IOException {
        handle(request, response, authentication);
        clearAuthenticationAttributes(request);
    }
 
	protected void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException {
    	CustomUserDetails loggedUser = (CustomUserDetails) authentication.getPrincipal();
    	HttpSession session = request.getSession();
    	session.setAttribute(SessionConstants.USER_NIU, loggedUser.getNiu());
    	
    	
        String targetUrl = Redirecter.determineTargetUrl(loggedUser);
        
        if(!targetUrl.equals("/index")) {
        	session.setAttribute(SessionConstants.USER_ROLE, RoleEnum.find(targetUrl.substring(1)).toString());
        }
 
        if (response.isCommitted()) {
            log.debug("Response has already been committed. Unable to redirect to " + targetUrl);
            return;
        }
 
        redirectStrategy.sendRedirect(request, response, targetUrl);
    }
 
    protected void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }
 
    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }
    
    protected RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }
}
