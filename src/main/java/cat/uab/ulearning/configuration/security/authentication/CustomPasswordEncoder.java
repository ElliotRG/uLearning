package cat.uab.ulearning.configuration.security.authentication;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Custom Password Encoder implementation to avoid
 * Spring Security default password encoding.
 * 
 * @author Elliot RG
 *
 */
public class CustomPasswordEncoder implements PasswordEncoder {

	@Override
	public String encode(CharSequence rawPassword) {
		return rawPassword.toString();
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		return rawPassword.toString().equals(encodedPassword);
	}

}
