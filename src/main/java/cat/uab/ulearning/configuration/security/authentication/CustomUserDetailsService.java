package cat.uab.ulearning.configuration.security.authentication;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cat.uab.ulearning.model.entities.User;
import cat.uab.ulearning.model.entities.UserSubject;
import cat.uab.ulearning.model.services.interfaces.UserService;
 
@Service
@Transactional
public class CustomUserDetailsService implements UserDetailsService {
	
	private static Logger log = LoggerFactory.getLogger(CustomUserDetailsService.class);
 
    @Autowired
    private UserService userService;
 
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userService.find(Integer.parseInt(username));
 
        if (user == null) {
            throw new UsernameNotFoundException("User " + username + " was not found in the database");
        }
        
        log.info("Found User: " + user);
        
        List<GrantedAuthority> grantList = new ArrayList<>();
        for (UserSubject userSubject : user.getUserSubjects()) {
            GrantedAuthority authority = new SimpleGrantedAuthority(userSubject.getUserRole());
            grantList.add(authority);
        }
 
        return new CustomUserDetails(user.getNiu(), user.getName(), user.getPassword(), grantList);
    }
 
}