package cat.uab.ulearning.configuration;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mobile.device.DeviceHandlerMethodArgumentResolver;
import org.springframework.mobile.device.DeviceResolverHandlerInterceptor;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.unit.DataSize;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableTransactionManagement
@EnableScheduling
public class BeansDefinition implements WebMvcConfigurer {

	@Value("${spring.servlet.multipart.max-file-size}")
	private DataSize maxFileSize;
	@Value("${spring.servlet.multipart.max-request-size}")
	private DataSize maxRequestSize;

	/** Hibernate Beans **/
	@Bean
	@Autowired
	public PlatformTransactionManager hibernateTransactionManager(FactoryBean<SessionFactory> sessionFactoryBean)
			throws Exception {
		HibernateTransactionManager htm = new HibernateTransactionManager();
		htm.setSessionFactory(sessionFactoryBean.getObject());
		return htm;
	}

	@Bean
	@Autowired
	public FactoryBean<SessionFactory> sessionFactory(DataSource dataSource) {
		LocalSessionFactoryBean fb = new LocalSessionFactoryBean();
		fb.setDataSource(dataSource);
		fb.setPackagesToScan("cat.uab.ulearning.model.*");
		fb.setPhysicalNamingStrategy(new PhysicalNamingStrategyStandardImpl());
		return fb;
	}

	/** Multipart Resolving Bean **/
	@Bean
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setMaxUploadSizePerFile(maxFileSize.toBytes());
		multipartResolver.setMaxUploadSize(maxRequestSize.toBytes());

		return multipartResolver;
	}

	/** Spring Mobile Device Resolving Beans **/
	@Bean
	public DeviceResolverHandlerInterceptor deviceResolverHandlerInterceptor() {
		return new DeviceResolverHandlerInterceptor();
	}

	@Bean
	public DeviceHandlerMethodArgumentResolver deviceHandlerMethodArgumentResolver() {
		return new DeviceHandlerMethodArgumentResolver();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(deviceResolverHandlerInterceptor());
	}

	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.add(deviceHandlerMethodArgumentResolver());
	}

}
