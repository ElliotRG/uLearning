package cat.uab.ulearning.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class DateTimeUtils {

    private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public static Date formatDate(Date rawDate) {
        try {
            return sdf.parse(sdf.format(rawDate));
        } catch (ParseException e) {
            log.error("Couldn't format {} to {} format", rawDate.toString(), sdf.toPattern());
            log.error(e.getMessage());
            
            return rawDate;
        }
    }
    
}