package cat.uab.ulearning.model.repositories.interfaces;

import cat.uab.ulearning.model.entities.UserSubject;

public interface UserSubjectRepository extends GenericDAO<UserSubject> {
	
}