package cat.uab.ulearning.model.repositories.interfaces;

import cat.uab.ulearning.model.entities.Answer;

public interface AnswerRepository extends GenericDAO<Answer> {

}