package cat.uab.ulearning.model.repositories.impl;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.TemporalType;

import org.springframework.stereotype.Repository;

import cat.uab.ulearning.model.entities.UserQuiz;
import cat.uab.ulearning.model.repositories.interfaces.UserQuizRepository;

@Repository
public class UserQuizRepositoryImpl extends GenericDAOImpl<UserQuiz> implements UserQuizRepository {
	
	public UserQuizRepositoryImpl() {
		setClazz(UserQuiz.class);
	}

	@Override
	public UserQuiz findByUserAndQuiz(int userNiu, int quizId) {
		return getSingleResultOrNull(getSession().createQuery("FROM " + getEntityName() 
			+ " WHERE user_niu = " + userNiu 
			+ " AND quiz_id = " + quizId, UserQuiz.class));
	}

	@Override
	public List<UserQuiz> findAllByUser(int studentNiu) {
		return getSession().createQuery("FROM " + getEntityName() 
			+ " WHERE user_niu = " + studentNiu, UserQuiz.class).getResultList();
	}

	@Override
	public List<UserQuiz> findAllByQuiz(int idQuiz) {
		return getSession().createQuery("FROM " + getEntityName() 
			+ " WHERE quiz_id = " + idQuiz, UserQuiz.class).getResultList();
	}

	@Override
	public List<UserQuiz> findCompletedByQuiz(int idQuiz) {
		return getSession().createQuery("FROM " + getEntityName() 
			+ " WHERE quiz_id = " + idQuiz
			+ " AND completed = true", 
			UserQuiz.class).getResultList();
	}

	@Override
	public List<UserQuiz> findLastAnsweredMoreThan3DaysAgo() {
		return getSession().createQuery("FROM " + getEntityName() 
			+ " WHERE last_answer_date <= :dueDate", UserQuiz.class)
			.setParameter("dueDate", LocalDateTime.now().minusDays(3), TemporalType.DATE)
			.getResultList();
	}

}