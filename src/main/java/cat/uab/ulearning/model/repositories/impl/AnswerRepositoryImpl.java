package cat.uab.ulearning.model.repositories.impl;

import org.springframework.stereotype.Repository;

import cat.uab.ulearning.model.entities.Answer;
import cat.uab.ulearning.model.repositories.interfaces.AnswerRepository;

@Repository
public class AnswerRepositoryImpl extends GenericDAOImpl<Answer> implements AnswerRepository {
	
	public AnswerRepositoryImpl() {
		setClazz(Answer.class);
	}

}
