package cat.uab.ulearning.model.repositories.interfaces;

import java.util.List;

import cat.uab.ulearning.model.entities.Quiz;

public interface QuizRepository extends GenericDAO<Quiz> {
	
	public List<Quiz> findQuizzesBySubject(int subjectCode, String userRole);

	public List<Quiz> findQuizzesFromAllUserSubjects(int userNiu, String userRole);

}