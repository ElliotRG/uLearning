package cat.uab.ulearning.model.repositories.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import cat.uab.ulearning.constants.ModelEnum;
import cat.uab.ulearning.constants.RoleEnum;
import cat.uab.ulearning.model.entities.Quiz;
import cat.uab.ulearning.model.repositories.interfaces.QuizRepository;

@Repository
public class QuizRepositoryImpl extends GenericDAOImpl<Quiz> implements QuizRepository {
	
	public QuizRepositoryImpl() {
		setClazz(Quiz.class);
	}

	@Override
	public List<Quiz> findQuizzesBySubject(int subjectCode, String userRole) {
		if (RoleEnum.STUDENT.toString().equals(userRole)) {
			return getSession().createQuery("FROM " + getEntityName() 
				+ " WHERE subject_code = " + subjectCode 
				+ " AND enabled = true"
				+ " ORDER BY expirationDate ASC", Quiz.class).list();
		} else if (RoleEnum.TEACHER.toString().equals(userRole)) {
			return getSession().createQuery("FROM " + getEntityName() 
				+ " WHERE subject_code = " + subjectCode 
				+ " ORDER BY lastModified DESC", Quiz.class).list();
		}
		
		return null;
	}

	@Override
	public List<Quiz> findQuizzesFromAllUserSubjects(int userNiu, String userRole) {
		if (RoleEnum.STUDENT.toString().equals(userRole)) {
			return getSession().createQuery("FROM " + getEntityName()
				+ " WHERE subject.code IN"
				+ " (SELECT subject.code"
				+ " FROM " + ModelEnum.USER_SUBJECT.toString()
				+ " WHERE user.niu = " + userNiu
				+ " AND userRole = '" + userRole + "')"
				+ " AND enabled = true "
				+ " ORDER BY expirationDate ASC", Quiz.class).list();
		} else if (RoleEnum.TEACHER.toString().equals(userRole)) {
			return getSession().createQuery("FROM " + getEntityName()
				+ " WHERE subject.code IN "
				+ " (SELECT subject.code" 
				+ " FROM " + ModelEnum.USER_SUBJECT.toString() 
				+ " WHERE user.niu = " + userNiu 
				+ " AND userRole = '" + userRole + "')"
				+ " ORDER BY lastModified DESC", Quiz.class).list();
		}

		return null;
	}

}
