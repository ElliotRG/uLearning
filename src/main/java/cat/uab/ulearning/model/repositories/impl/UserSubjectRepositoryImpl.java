package cat.uab.ulearning.model.repositories.impl;

import org.springframework.stereotype.Repository;

import cat.uab.ulearning.model.entities.UserSubject;
import cat.uab.ulearning.model.repositories.interfaces.UserSubjectRepository;

@Repository
public class UserSubjectRepositoryImpl extends GenericDAOImpl<UserSubject> implements UserSubjectRepository {
	
	public UserSubjectRepositoryImpl() {
		setClazz(UserSubject.class);
	}

}
