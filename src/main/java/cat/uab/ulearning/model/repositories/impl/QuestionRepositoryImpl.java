package cat.uab.ulearning.model.repositories.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import cat.uab.ulearning.constants.ModelEnum;
import cat.uab.ulearning.model.entities.Question;
import cat.uab.ulearning.model.repositories.interfaces.QuestionRepository;

@Repository
public class QuestionRepositoryImpl extends GenericDAOImpl<Question> implements QuestionRepository {
	
	public QuestionRepositoryImpl() {
		setClazz(Question.class);
	}

	@Override
	public List<Question> getQuestionsForUser(int quizId, int userNiu) {
		return getSession().createQuery("FROM " + getEntityName() + " q"
			+ " WHERE q.quiz.idQuiz = " + quizId
			+ " AND q.idQuestion NOT IN"
			+ " (SELECT uq.question.idQuestion"
			+ " FROM " + ModelEnum.USER_QUESTION.getEntityName() + " uq"
			+ " WHERE uq.correctlyAnswered = true)", Question.class).list();
	}

}
