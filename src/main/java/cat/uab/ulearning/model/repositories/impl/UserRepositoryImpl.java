package cat.uab.ulearning.model.repositories.impl;

import org.springframework.stereotype.Repository;

import cat.uab.ulearning.model.entities.User;
import cat.uab.ulearning.model.repositories.interfaces.UserRepository;

@Repository
public class UserRepositoryImpl extends GenericDAOImpl<User> implements UserRepository {
 
   public UserRepositoryImpl() {
      setClazz(User.class);
   }

}
