package cat.uab.ulearning.model.repositories.interfaces;

import java.util.List;

import cat.uab.ulearning.model.entities.Question;

public interface QuestionRepository extends GenericDAO<Question> {

	public List<Question> getQuestionsForUser(int quizId, int userNiu);

}
