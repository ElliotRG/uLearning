package cat.uab.ulearning.model.repositories.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import cat.uab.ulearning.model.entities.Subject;
import cat.uab.ulearning.model.entities.UserSubject;
import cat.uab.ulearning.model.repositories.interfaces.SubjectRepository;

@Repository
public class SubjectRepositoryImpl extends GenericDAOImpl<Subject> implements SubjectRepository {

	public SubjectRepositoryImpl() {
		setClazz(Subject.class);
	}

	@Override
	public List<Subject> findSubjectsByUser(int userNiu) {
		return getSession().createQuery("FROM " + getEntityName() + " WHERE code IN " + "(SELECT subject.code FROM "
				+ UserSubject.class.getSimpleName() + " WHERE user.niu = " + userNiu + ") "
						+ "ORDER BY shortName", Subject.class).list();
	}

}
