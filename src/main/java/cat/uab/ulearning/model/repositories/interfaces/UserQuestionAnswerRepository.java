package cat.uab.ulearning.model.repositories.interfaces;

import java.util.List;

import cat.uab.ulearning.model.entities.UserQuestionAnswer;

public interface UserQuestionAnswerRepository extends GenericDAO<UserQuestionAnswer> {

	public List<UserQuestionAnswer> findUserQuestionAnswersByQuestion(int idUserQuestion);
	
}