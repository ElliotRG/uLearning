package cat.uab.ulearning.model.repositories.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import cat.uab.ulearning.model.entities.UserQuestionAnswer;
import cat.uab.ulearning.model.repositories.interfaces.UserQuestionAnswerRepository;

@Repository
public class UserQuestionAnswerRepositoryImpl extends GenericDAOImpl<UserQuestionAnswer> implements UserQuestionAnswerRepository {
	
	public UserQuestionAnswerRepositoryImpl() {
		setClazz(UserQuestionAnswer.class);
	}

	@Override
	public List<UserQuestionAnswer> findUserQuestionAnswersByQuestion(int idUserQuestion) {
		return getSession().createQuery("FROM " + getEntityName() 
			+ " WHERE userQuestion.idUserQuestion = " + idUserQuestion, 
			UserQuestionAnswer.class).getResultList();
	}

}
