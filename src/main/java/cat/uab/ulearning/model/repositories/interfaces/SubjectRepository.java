package cat.uab.ulearning.model.repositories.interfaces;

import java.util.List;

import cat.uab.ulearning.model.entities.Subject;

public interface SubjectRepository extends GenericDAO<Subject> {
	
	List<Subject> findSubjectsByUser(int userNiu);

}