package cat.uab.ulearning.model.repositories.interfaces;

import java.util.List;

import cat.uab.ulearning.model.entities.UserQuiz;

public interface UserQuizRepository extends GenericDAO<UserQuiz> {

	public UserQuiz findByUserAndQuiz(int userNiu, int quizId);

	public List<UserQuiz> findAllByUser(int studentNiu);

	public List<UserQuiz> findAllByQuiz(int idQuiz);

	public List<UserQuiz> findCompletedByQuiz(int idQuiz);

	public List<UserQuiz> findLastAnsweredMoreThan3DaysAgo();
	
}