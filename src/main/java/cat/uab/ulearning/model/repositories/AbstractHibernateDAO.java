package cat.uab.ulearning.model.repositories;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import lombok.Setter;

/**
 * This class abstracts the CRUD operations that are used by
 * Hibernate's Session and can be applied to any entity T.
 *   
 * @author Elliot RG
 *
 */
@Repository
public abstract class AbstractHibernateDAO<T extends Serializable> {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 5174323124009012794L;

	@Setter
	private Class<T> clazz;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public T find(int id) {
		return getSession().find(clazz, id);
	}
	
	public List<T> findAll() {
		return getSession().createQuery( "FROM " + getEntityName(), clazz).list();
	}
	
	public void save(T entity) {
		try {
			getSession().saveOrUpdate(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveList(Collection<T> entities) {
		for(T entity : entities) {
			save(entity);
		}
	}
	
	public void delete(T entity) {
		try {
			getSession().delete(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void deleteList(Collection<T> entities) {
		for(T entity : entities) {
			getSession().delete(entity);
		}
	}
	
	protected final Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	protected String getEntityName() {
		return clazz.getSimpleName();
	}
	
}
