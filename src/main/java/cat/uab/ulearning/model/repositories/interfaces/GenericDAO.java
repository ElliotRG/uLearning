package cat.uab.ulearning.model.repositories.interfaces;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.hibernate.query.Query;

/**
 * Interface for GenericDAO implementation
 * 
 * @author Elliot RG
 *
 */
public interface GenericDAO<T extends Serializable> {
	
	T find(int id);
	
	List<T> findAll();
	
	void save(final T entity);
	
	void saveList(Collection<T> entities);
	
	void delete(final T entity);
	
	void deleteList(Collection<T> entities);
	
    T getSingleResultOrNull(Query<T> query);

}
