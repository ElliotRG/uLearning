package cat.uab.ulearning.model.repositories.interfaces;

import java.util.List;

import cat.uab.ulearning.model.entities.UserQuestion;

public interface UserQuestionRepository extends GenericDAO<UserQuestion> {

	public List<UserQuestion> findCorrectlyAnsweredByUserAndQuiz(int userNiu, int quizId);

	public UserQuestion findByUserAndQuestion(int userNiu, int questionId);

}