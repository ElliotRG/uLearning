package cat.uab.ulearning.model.repositories.interfaces;

import cat.uab.ulearning.model.entities.User;

public interface UserRepository extends GenericDAO<User> {
    
}