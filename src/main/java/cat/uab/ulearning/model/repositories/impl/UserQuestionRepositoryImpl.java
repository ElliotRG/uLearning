package cat.uab.ulearning.model.repositories.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import cat.uab.ulearning.constants.ModelEnum;
import cat.uab.ulearning.model.entities.UserQuestion;
import cat.uab.ulearning.model.repositories.interfaces.UserQuestionRepository;

@Repository
public class UserQuestionRepositoryImpl extends GenericDAOImpl<UserQuestion> implements UserQuestionRepository {
	
	public UserQuestionRepositoryImpl() {
		setClazz(UserQuestion.class);
	}

	@Override
	public List<UserQuestion> findCorrectlyAnsweredByUserAndQuiz(int userNiu, int quizId) {
		return getSession().createQuery("FROM " + getEntityName()
			+ " WHERE correctlyAnswered = true"
			+ " AND user.niu = " + userNiu
			+ " AND question.idQuestion IN" 
			+ " (SELECT idQuestion"
			+ " FROM " + ModelEnum.QUESTION.getEntityName()
			+ " WHERE quiz.idQuiz = " + quizId + ")", 
			UserQuestion.class).getResultList();
	}

	@Override
	public UserQuestion findByUserAndQuestion(int userNiu, int questionId) {
		return getSession().createQuery("FROM " + getEntityName() 
			+ " WHERE user.niu = " + userNiu
			+ " AND question.idQuestion = " + questionId, UserQuestion.class)
			.getResultStream().findFirst().orElse(null);
	}

}
