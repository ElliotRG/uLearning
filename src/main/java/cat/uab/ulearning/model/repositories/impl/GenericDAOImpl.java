package cat.uab.ulearning.model.repositories.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.query.Query;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import cat.uab.ulearning.model.repositories.AbstractHibernateDAO;
import cat.uab.ulearning.model.repositories.interfaces.GenericDAO;

/**
 * Implementation of a GenericDAO that will be extended by
 * any entity T. This GenericDAO inherits the AbstractHibernateDAO
 * CRUD operations methods.
 * 
 * @author Elliot RG
 *
 */
@Repository
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public abstract class GenericDAOImpl<T extends Serializable>
  extends AbstractHibernateDAO<T> implements GenericDAO<T> {

	// Inherits AbstractHibernateDAO methods
	
	/**
	 * Gets a single result result or null from the result list
	 * returned by the given query execution.
	 * 
	 * @param query - The query to get the single row from.
	 * @return The entity object corresponding to the single row.
	 */
	@Override
	public T getSingleResultOrNull(Query<T> query) {
        List<T> results = query.getResultList();
        return results.isEmpty() ? null : results.get(0); 
	}
	
}
