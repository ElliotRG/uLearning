package cat.uab.ulearning.model.accessors;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QuizDetailsAccessor {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public byte[] getQuizSubEntityImage(String entityName, int entityId) {
		return (byte[]) sessionFactory.getCurrentSession().createSQLQuery("SELECT image FROM " 
				+ entityName + " WHERE id_" + entityName + " = " + entityId).getSingleResult();
	}

}
