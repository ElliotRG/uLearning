package cat.uab.ulearning.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(exclude = "question")
@Entity
@Table(name = "answer", uniqueConstraints = @UniqueConstraint (columnNames = "id_answer"))
public class Answer implements Serializable {

	private static final long serialVersionUID = -5757428827953241843L;
	
	@Id
	@Column(name = "id_answer")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NotNull
	private int idAnswer;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "question_id", referencedColumnName = "id_question", nullable = false)
	private Question question;
	
	@Column(name = "answer")
	@NotNull
	private String answer;
	
	@Lob
	@Column(name = "image")
	private Byte[] image;

	@Column(name = "image_path")
	private String imagePath;
	
	@Column(name = "correct")
	@NotNull
	private boolean correct;

}
