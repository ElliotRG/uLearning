package cat.uab.ulearning.model.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString(exclude = "userSubjects")
@Entity
@Table(name = "user", uniqueConstraints = @UniqueConstraint (columnNames="niu"))
public class User implements Serializable {

	private static final long serialVersionUID = -732446745224222119L;

	@Id
    @Column(name = "niu")
	@NotNull
    private int niu;
    
    @Column(name = "name")
    @NotNull
    private String name;
    
    @Column(name = "password")
    @NotNull
    private String password;
    
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<UserSubject> userSubjects = new HashSet<UserSubject>();
    
}
