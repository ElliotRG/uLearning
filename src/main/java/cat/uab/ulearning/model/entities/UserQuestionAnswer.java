package cat.uab.ulearning.model.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "user_question_answer", uniqueConstraints = @UniqueConstraint(columnNames = "id_user_question_answer"))
public class UserQuestionAnswer implements Serializable { 

	private static final long serialVersionUID = -9192987981745680107L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user_question_answer")
    private int idUserQuestionAnswer;
	
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_question_id", referencedColumnName = "id_user_question", nullable = false)
    private UserQuestion userQuestion;
    
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "answer_id", referencedColumnName = "id_answer", nullable = false)
    private Answer answer;
    
}
