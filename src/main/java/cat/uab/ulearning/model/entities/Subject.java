package cat.uab.ulearning.model.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(exclude = {"userSubjects", "quizzes"})
@Entity
@Table(name = "subject", uniqueConstraints = @UniqueConstraint (columnNames = "code"))
public class Subject implements Serializable {
	
	private static final long serialVersionUID = 663148873882508278L;

	@Id
    @Column(name = "code")
	@NotNull
    private int code;
    
    @Column(name = "long_name")
    @NotNull
    private String longName;
    
    @Column(name = "short_name")
    @NotNull
    private String shortName;
    
    @Column(name = "degree")
    @NotNull
    private String degree;
    
    @OneToMany(mappedBy = "subject", fetch = FetchType.LAZY)
    @Fetch(FetchMode.JOIN)
    @JsonIgnore
    private Set<UserSubject> userSubjects = new HashSet<>();
    
    @JsonIgnore
    @OneToMany(mappedBy = "subject", fetch = FetchType.LAZY)
    private Set<Quiz> quizzes = new HashSet<>();

}
