package cat.uab.ulearning.model.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "user_quiz", uniqueConstraints = @UniqueConstraint(columnNames = "id_user_quiz"))
public class UserQuiz implements Serializable {

	private static final long serialVersionUID = -6583427448892354829L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user_quiz")
    private int idUserQuiz;
	
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_niu", referencedColumnName = "niu", nullable = false)
    private User user;
    
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "quiz_id", referencedColumnName = "id_quiz", nullable = false)
    private Quiz quiz;

    @Column(name = "score")
    @NotNull
    private int score;

    @Column(name = "last_answer_date")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date lastAnswerDate;

    @Column(name = "completed")
    @NotNull
    private boolean completed;
    
}
