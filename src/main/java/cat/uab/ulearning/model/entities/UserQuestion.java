package cat.uab.ulearning.model.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "user_question", uniqueConstraints = @UniqueConstraint(columnNames = "id_user_question"))
public class UserQuestion implements Serializable { 

	private static final long serialVersionUID = 424340534980578580L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user_question")
    private int idUserQuestion;
	
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_niu", referencedColumnName = "niu", nullable = false)
    private User user;
    
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "question_id", referencedColumnName = "id_question", nullable = false)
    private Question question;
 
    @Column(name = "correctly_answered")
    @NotNull
    private boolean correctlyAnswered;

    @Column(name = "start_date")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date startDate;

    public UserQuestion() {
        this.startDate = new Date();
    }

    public UserQuestion(User user, Question question) {
        this();
        this.user = user;
        this.question = question;
    }
    
}
