package cat.uab.ulearning.model.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.format.annotation.DateTimeFormat;

import cat.uab.ulearning.constants.QuestionScoring;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(exclude = { "subject", "questions" })
@Entity
@Table(name = "quiz", uniqueConstraints = @UniqueConstraint (columnNames = "id_quiz"))
public class Quiz implements Serializable {

	private static final long serialVersionUID = 7899292643417556205L;
	
	@Id
	@Column(name = "id_quiz")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NotNull
	private int idQuiz;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "subject_code", referencedColumnName = "code", nullable = false)	
	private Subject subject;
	
	@OneToMany(mappedBy = "quiz", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@Fetch(FetchMode.SELECT)
	@JsonIgnore
	private List<Question> questions = new LinkedList<>();

	@Column(name = "title")
	@NotNull
	private String title;
	
	@Column(name = "topic")
	private String topic;
	
	@Column(name = "subtopic")
	private String subtopic;
	
	@Column(name = "expiration_date")
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private Date expirationDate;

	@Column(name = "last_modified")
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private Date lastModified;

	@Column(name = "total_score")
	@NotNull
	private int totalScore;

	@Column(name = "enabled")
	@NotNull
	private boolean enabled;

    public void addQuestion(Question question) {
        this.questions.add(question);
        question.setQuiz(this);
    }
 
    public void removeQuestion(Question question) {
		this.questions.remove(question);
		question.setQuiz(null);
    }
    
    public Question getQuestionById(int questionId) {
    	Question question = null;
    	int i = 0;
    	while(question == null && i < this.questions.size()) {
    		if(this.questions.get(i).getIdQuestion() == questionId) {
    			question = this.questions.get(i); 
    		}
    		i++;
    	}
    	
    	return question;
	}
	
	public void recalculateTotalScore() {
		this.totalScore = 0;
		for(Question question : this.questions) {
			this.totalScore += QuestionScoring.getScoreByDifficulty(question.getDifficulty());
		}
	}
    
}
