package cat.uab.ulearning.model.entities;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(exclude = { "quiz", "answers" })
@Entity
@Table(name = "question", uniqueConstraints = @UniqueConstraint(columnNames = "id_question"))
public class Question implements Serializable {

	private static final long serialVersionUID = -8286865818098612133L;

	@Id
	@Column(name = "id_question")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NotNull
	private int idQuestion;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quiz_id", referencedColumnName = "id_quiz", nullable = false)
	private Quiz quiz;

	@OneToMany(mappedBy = "question", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@Fetch(FetchMode.SELECT)
	@JsonIgnore
	private List<Answer> answers = new LinkedList<>();

	@Column(name = "question")
	@NotNull
	private String question;

	@Lob
	@Column(name = "image")
	private Byte[] image;

	@Column(name = "image_path")
	private String imagePath;

	@Column(name = "feedback")
	private String feedback;

	@Column(name = "difficulty")
	@NotNull
	private int difficulty;

	@Column(name = "time_to_answer")
	@NotNull
	@DateTimeFormat(pattern = "HH:mm:ss")
	private LocalTime timeToAnswer;

	public Question() {
		for (int i = 0; i < 4; i++) {
			addAnswer(new Answer());
		}
	}

	public void addAnswer(Answer answer) {
		this.answers.add(answer);
		answer.setQuestion(this);
	}

	public Answer getAnswerById(int answerId) {
		Answer answer = null;
		int i = 0;
		while (answer == null && i < this.answers.size()) {
			if (this.answers.get(i).getIdAnswer() == answerId) {
				answer = this.answers.get(i);
			}
			i++;
		}

		return answer;
	}

	/**
	 * We override the equals method because when saving in
	 * edit mode we consider a question as different only if 
	 * its ID doesn't match with the question ID of the already
	 * existing quiz, so in that case the question may have been removed.
	 */
	@Override
	public boolean equals(Object obj) {
        if (!(obj instanceof Question)) {
            return false;
        }
		
        return ((Question) obj).getIdQuestion() == this.getIdQuestion();
	}

}
