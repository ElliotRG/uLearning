package cat.uab.ulearning.model.dto;

import java.util.List;

import lombok.Data;

@Data
public class QuestionAnswersDTO {
	
	private int questionId;
	private List<Integer> answersIds;
	
}
