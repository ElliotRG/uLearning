package cat.uab.ulearning.model.services.impl;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cat.uab.ulearning.model.entities.UserSubject;
import cat.uab.ulearning.model.repositories.interfaces.UserSubjectRepository;
import cat.uab.ulearning.model.services.interfaces.UserSubjectService;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@Log4j2
public class UserSubjectServiceImpl extends GenericServiceImpl<UserSubject> implements UserSubjectService {
	
	@Autowired
	private UserSubjectRepository userSubjectRepository;
	
	@Override
	protected Logger getLogger() {
		return log;
	}

	@Override
	protected UserSubjectRepository getDAO() {
		return this.userSubjectRepository;
	}

    @Autowired
    public UserSubjectServiceImpl(UserSubjectRepository userSubjectRepository) {
        this.userSubjectRepository = userSubjectRepository;
    }
	
}
