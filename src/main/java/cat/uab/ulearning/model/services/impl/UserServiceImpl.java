package cat.uab.ulearning.model.services.impl;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cat.uab.ulearning.model.entities.User;
import cat.uab.ulearning.model.repositories.interfaces.UserRepository;
import cat.uab.ulearning.model.services.interfaces.UserService;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@Log4j2
public class UserServiceImpl extends GenericServiceImpl<User> implements UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder customPasswordEncoder;
	
	@Override
	protected Logger getLogger() {
		return log;
	}

	@Override
	protected UserRepository getDAO() {
		return this.userRepository;
	}

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

	@Override
	public void save(User user) {
    	user.setPassword(user.getPassword());
    	user.setPassword(customPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
	}

}