package cat.uab.ulearning.model.services.interfaces;

import cat.uab.ulearning.model.entities.User;

public interface UserService extends GenericService<User> {
	
}
