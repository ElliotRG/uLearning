package cat.uab.ulearning.model.services.impl;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cat.uab.ulearning.model.entities.Answer;
import cat.uab.ulearning.model.repositories.interfaces.AnswerRepository;
import cat.uab.ulearning.model.services.interfaces.AnswerService;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@Log4j2
public class AnswerServiceImpl extends GenericServiceImpl<Answer> implements AnswerService {
	
	@Autowired
	private AnswerRepository answerRepository;
	
	@Override
	protected Logger getLogger() {
		return log;
	}

	@Override
	protected AnswerRepository getDAO() {
		return this.answerRepository;
	}

    @Autowired
    public AnswerServiceImpl(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }
	
}
