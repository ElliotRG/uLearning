package cat.uab.ulearning.model.services.interfaces;

import java.util.List;
import java.util.Map;

import cat.uab.ulearning.model.entities.UserQuiz;

public interface UserQuizService extends GenericService<UserQuiz> {
	
	public UserQuiz getUserQuizByUserAndQuiz(int userNiu, int quizId);

	public Map<Integer, UserQuiz> getUserQuizzesIdQuizMap(int studentNiu);

	public void updateUserQuizzesCompletion(int idQuiz, int updatedQuizTotalScore, boolean revokeCompletion);

	public List<UserQuiz> getUserQuizzesForDecayment();
	
}