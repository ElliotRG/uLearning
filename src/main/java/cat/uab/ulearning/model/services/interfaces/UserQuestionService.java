package cat.uab.ulearning.model.services.interfaces;

import java.util.List;

import cat.uab.ulearning.model.entities.Question;
import cat.uab.ulearning.model.entities.User;
import cat.uab.ulearning.model.entities.UserQuestion;

public interface UserQuestionService extends GenericService<UserQuestion> {

	public List<UserQuestion> getAnsweredUserQuestionsByUserAndQuiz(int userNiu, int userQuizId);

	public UserQuestion getUserQuestionByUserAndQuestion(int userNiu, int questionId);

	public UserQuestion getOrCreate(User user, Question question);

}