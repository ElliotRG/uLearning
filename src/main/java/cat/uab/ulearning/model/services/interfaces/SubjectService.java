package cat.uab.ulearning.model.services.interfaces;

import java.util.List;

import cat.uab.ulearning.model.entities.Subject;

public interface SubjectService extends GenericService<Subject> {
	
	public List<Subject> getSubjectsByUser(int userNiu);

}
