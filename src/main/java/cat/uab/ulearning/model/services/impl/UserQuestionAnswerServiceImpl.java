package cat.uab.ulearning.model.services.impl;

import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cat.uab.ulearning.model.entities.UserQuestionAnswer;
import cat.uab.ulearning.model.repositories.interfaces.UserQuestionAnswerRepository;
import cat.uab.ulearning.model.services.interfaces.UserQuestionAnswerService;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@Log4j2
public class UserQuestionAnswerServiceImpl extends GenericServiceImpl<UserQuestionAnswer> implements UserQuestionAnswerService {
	
	@Autowired
	private UserQuestionAnswerRepository userQuestionAnswerRepository;
	
	@Override
	protected Logger getLogger() {
		return log;
	}

	@Override
	protected UserQuestionAnswerRepository getDAO() {
		return this.userQuestionAnswerRepository;
	}

    @Autowired
    public UserQuestionAnswerServiceImpl(UserQuestionAnswerRepository userQuestionAnswerRepository) {
        this.userQuestionAnswerRepository = userQuestionAnswerRepository;
    }

	@Override
	public List<Integer> getUserQuestionAnswersIdsByQuestion(int idUserQuestion) {
		List<UserQuestionAnswer> userQuestionAnswers = userQuestionAnswerRepository
				.findUserQuestionAnswersByQuestion(idUserQuestion);
		List<Integer> userQuestionAnswersIds = new LinkedList<>();

		for(UserQuestionAnswer userQuestionAnswer : userQuestionAnswers) {
			userQuestionAnswersIds.add(userQuestionAnswer.getAnswer().getIdAnswer());
		}
		
		return userQuestionAnswersIds;
	}
	
}
