package cat.uab.ulearning.model.services.impl;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cat.uab.ulearning.constants.ModelEnum;
import cat.uab.ulearning.model.accessors.QuizDetailsAccessor;
import cat.uab.ulearning.model.entities.Quiz;
import cat.uab.ulearning.model.repositories.interfaces.QuizRepository;
import cat.uab.ulearning.model.services.interfaces.QuizService;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@Log4j2
public class QuizServiceImpl extends GenericServiceImpl<Quiz> implements QuizService {
	
	@Autowired
	private QuizRepository quizRepository;
	
	@Autowired
	private QuizDetailsAccessor quizDetailsAccessor;
	
	@Override
	protected Logger getLogger() {
		return log;
	}

	@Override
	protected QuizRepository getDAO() {
		return this.quizRepository;
	}

    @Autowired
    public QuizServiceImpl(QuizRepository quizRepository) {
        this.quizRepository = quizRepository;
    }

	@Override
	public List<Quiz> getQuizzesBySubject(int subjectCode, String userRole) {
		List<Quiz> quizzes = quizRepository.findQuizzesBySubject(subjectCode, userRole);
		if(quizzes == null) {
			log.warn("No quizzes exist for given subject code");
		}
		return quizzes;
	}

	@Override
	public List<Quiz> getQuizzesFromAllUserSubjects(int userNiu, String userRole) {
		List<Quiz> quizzes = quizRepository.findQuizzesFromAllUserSubjects(userNiu, userRole);
		if(quizzes == null) {
			log.warn("No quizzes exist for given user role");
		}
		return quizzes;
	}

	@Override
	public void updateQuizPublication(int idQuiz, boolean publish) {
		Quiz quiz = this.find(idQuiz);
		quiz.setEnabled(publish);
		this.save(quiz);
	}

	@Override
	public byte[] getQuizSubEntityImage(String entityName, int entityId) {
		if(ModelEnum.find(entityName) != null) {
			return quizDetailsAccessor.getQuizSubEntityImage(entityName.toLowerCase(), entityId);
		}
		return null;
	}
    
}
