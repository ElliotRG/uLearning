package cat.uab.ulearning.model.services.interfaces;

import java.util.List;

import cat.uab.ulearning.model.entities.UserQuestionAnswer;

public interface UserQuestionAnswerService extends GenericService<UserQuestionAnswer> {

	public List<Integer> getUserQuestionAnswersIdsByQuestion(int idUserQuestion);

}