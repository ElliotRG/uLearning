package cat.uab.ulearning.model.services.interfaces;

import cat.uab.ulearning.model.entities.Answer;

public interface AnswerService extends GenericService<Answer> {

}
