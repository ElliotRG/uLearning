package cat.uab.ulearning.model.services.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cat.uab.ulearning.model.entities.UserQuiz;
import cat.uab.ulearning.model.repositories.interfaces.UserQuizRepository;
import cat.uab.ulearning.model.services.interfaces.UserQuizService;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@Log4j2
public class UserQuizServiceImpl extends GenericServiceImpl<UserQuiz> implements UserQuizService {
	
	@Autowired
	private UserQuizRepository userQuizRepository;
	
	@Override
	protected Logger getLogger() {
		return log;
	}

	@Override
	protected UserQuizRepository getDAO() {
		return this.userQuizRepository;
	}

    @Autowired
    public UserQuizServiceImpl(UserQuizRepository userQuizRepository) {
        this.userQuizRepository = userQuizRepository;
    }

	@Override
	public UserQuiz getUserQuizByUserAndQuiz(int userNiu, int quizId) {
		return userQuizRepository.findByUserAndQuiz(userNiu, quizId);
	}

	@Override
	public Map<Integer, UserQuiz> getUserQuizzesIdQuizMap(int studentNiu) {
		List<UserQuiz> userQuizzes = userQuizRepository.findAllByUser(studentNiu);
		Map<Integer, UserQuiz> userQuizzesIdMap = new HashMap<>();
		for (UserQuiz userQuiz : userQuizzes) {
			userQuizzesIdMap.put(userQuiz.getQuiz().getIdQuiz(), userQuiz);
		}
		
		return userQuizzesIdMap;
	}

	@Override
	public void updateUserQuizzesCompletion(int idQuiz, int updatedQuizTotalScore, boolean revokeCompletion) {
		List<UserQuiz> userQuizzes = userQuizRepository.findAllByQuiz(idQuiz);

		userQuizzes.parallelStream().forEach(userQuiz -> {
			if (userQuiz.getScore() >= updatedQuizTotalScore) {
				userQuiz.setCompleted(true);
			} else if (revokeCompletion) {
				// We only revoke the completion if the score is under the new totalScore.
				// This way, if the teacher has removed questions and therefore the totalScore
				// has decreased, the students who completed the quiz and whose score is still over 
				// the current one will preserve the quiz completion achievement.
				userQuiz.setCompleted(false);

				// We update the student last answer date to the quiz to now in order
				// to be fair with those who had completed the quiz some time ago
				// and the decayment service may think they haven't worked enough.
				// This way, even if the teacher decides to revoke their quiz completion,
				// the students will not be targeted by the decayment service for some days,
				// hopefully enough for them to realise teacher's revokation and work again
				// before the service does its work :)
				userQuiz.setLastAnswerDate(new Date());
			}
		});
		
		// We only perform the DB operation if we have something to update
		if (!userQuizzes.isEmpty()) {
			userQuizRepository.saveList(userQuizzes);
		}
	}

	@Override
	public List<UserQuiz> getUserQuizzesForDecayment() {
		return userQuizRepository.findLastAnsweredMoreThan3DaysAgo();
	}
	
}