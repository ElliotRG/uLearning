package cat.uab.ulearning.model.services.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cat.uab.ulearning.model.entities.Question;
import cat.uab.ulearning.model.entities.User;
import cat.uab.ulearning.model.entities.UserQuestion;
import cat.uab.ulearning.model.repositories.interfaces.UserQuestionRepository;
import cat.uab.ulearning.model.services.interfaces.UserQuestionService;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@Log4j2
public class UserQuestionServiceImpl extends GenericServiceImpl<UserQuestion> implements UserQuestionService {

	@Autowired
	private UserQuestionRepository userQuestionRepository;

	@Override
	protected Logger getLogger() {
		return log;
	}

	@Override
	protected UserQuestionRepository getDAO() {
		return this.userQuestionRepository;
	}

	@Autowired
	public UserQuestionServiceImpl(UserQuestionRepository userQuestionRepository) {
		this.userQuestionRepository = userQuestionRepository;
	}

	@Override
	public List<UserQuestion> getAnsweredUserQuestionsByUserAndQuiz(int userNiu, int quizId) {
		return userQuestionRepository.findCorrectlyAnsweredByUserAndQuiz(userNiu, quizId);
	}

	@Override
	public UserQuestion getUserQuestionByUserAndQuestion(int userNiu, int questionId) {
		return userQuestionRepository.findByUserAndQuestion(userNiu, questionId);
	}

	@Override
	public UserQuestion getOrCreate(User user, Question question) {
		UserQuestion userQuestion = getUserQuestionByUserAndQuestion(user.getNiu(), question.getIdQuestion());
		
		if (userQuestion == null) {
			userQuestion = new UserQuestion(user, question);
			userQuestionRepository.save(userQuestion);
		} else {
			userQuestion.setStartDate(new Date());
		}
		
		return userQuestion;
	}

}
