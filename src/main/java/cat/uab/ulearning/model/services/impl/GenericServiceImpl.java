package cat.uab.ulearning.model.services.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cat.uab.ulearning.model.repositories.interfaces.GenericDAO;
import cat.uab.ulearning.model.services.interfaces.GenericService;

/**
 * Implementation of a GenericService that will be extended by
 * any entity T. This service implements the methods that call
 * DAO's CRUD operations methods.
 * 
 * @author Elliot RG
 *
 */
@Service
@Transactional
public abstract class GenericServiceImpl<T extends Serializable> implements GenericService<T> {
	
	protected abstract Logger getLogger();
	
	protected abstract GenericDAO<T> getDAO();
	
	@Override
	@Transactional
	public T find(int id) {
		T entity = null;
		
		try {
			entity = getDAO().find(id);
		} catch (Exception e) {
			getLogger().error(e);
		}
		
		return entity;
	}

	@Override
	@Transactional
	public List<T> findAll() {
		List<T> entity = null;
		
		try {
			entity = getDAO().findAll();
		} catch (Exception e) {
			getLogger().error(e);
		}
		
		return entity;
	}

	@Override
	@Transactional
	public void save(T entity) {
		try {
			getDAO().save(entity);
		} catch (Exception e) {
			getLogger().error(e);
		}
	}

	@Override
	@Transactional
	public void saveList(List<T> entities) {
		try {
			getDAO().saveList(entities);
		} catch (Exception e) {
			getLogger().error(e);
		}
	}

	@Override
	@Transactional
	public void delete(T entity) {
		try {
			getDAO().delete(entity);
		} catch (Exception e) {
			getLogger().error(e);
		}
	}

	@Override
	@Transactional
	public void deleteList(List<T> entities) {
		try {
			getDAO().deleteList(entities);
		} catch (Exception e) {
			getLogger().error(e);
		}
	}
	
}
