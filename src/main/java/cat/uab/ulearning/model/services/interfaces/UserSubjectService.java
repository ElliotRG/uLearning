package cat.uab.ulearning.model.services.interfaces;

import cat.uab.ulearning.model.entities.UserSubject;

public interface UserSubjectService extends GenericService<UserSubject> {

}
