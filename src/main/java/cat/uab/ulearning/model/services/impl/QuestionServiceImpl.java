package cat.uab.ulearning.model.services.impl;

import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cat.uab.ulearning.model.entities.Question;
import cat.uab.ulearning.model.repositories.interfaces.QuestionRepository;
import cat.uab.ulearning.model.services.interfaces.QuestionService;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@Log4j2
public class QuestionServiceImpl extends GenericServiceImpl<Question> implements QuestionService {
	
	@Autowired
	private QuestionRepository questionRepository;
	
	@Override
	protected Logger getLogger() {
		return log;
	}

	@Override
	protected QuestionRepository getDAO() {
		return this.questionRepository;
	}

    @Autowired
    public QuestionServiceImpl(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
	}

	@Override
	public Question fetchNextQuestion(int quizId, int userNiu) {
		List<Question> questionsForUser = questionRepository.getQuestionsForUser(quizId, userNiu);

		if(questionsForUser.isEmpty()) {
			return null;
		}
		
		return questionsForUser.get(new Random().nextInt(questionsForUser.size()));
	}
	
}
