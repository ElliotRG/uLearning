package cat.uab.ulearning.model.services.interfaces;

import cat.uab.ulearning.model.entities.Question;

public interface QuestionService extends GenericService<Question> {

    public Question fetchNextQuestion(int quizId, int userNiu);

}
