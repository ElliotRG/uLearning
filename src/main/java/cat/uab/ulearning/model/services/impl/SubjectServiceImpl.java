package cat.uab.ulearning.model.services.impl;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cat.uab.ulearning.model.entities.Subject;
import cat.uab.ulearning.model.repositories.interfaces.SubjectRepository;
import cat.uab.ulearning.model.services.interfaces.SubjectService;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@Log4j2
public class SubjectServiceImpl extends GenericServiceImpl<Subject> implements SubjectService {

	@Autowired
	private SubjectRepository subjectRepository;
	
	@Override
	protected Logger getLogger() {
		return log;
	}

	@Override
	protected SubjectRepository getDAO() {
		return this.subjectRepository;
	}

    @Autowired
    public SubjectServiceImpl(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

	@Override
	public List<Subject> getSubjectsByUser(int userNiu) {
		return subjectRepository.findSubjectsByUser(userNiu);
	}
  
}
