package cat.uab.ulearning.model.services.interfaces;

import java.io.Serializable;
import java.util.List;


/**
 * Interface for GenericService implementation
 * 
 * @author Elliot RG
 *
 */
public interface GenericService<T extends Serializable> {
	
	public T find(int id);
	
	public List<T> findAll();
	
	public void save(final T entity);
	
	public void saveList(List<T> entities);
	
	public void delete(final T entity);
	
	public void deleteList(List<T> entities);

}
