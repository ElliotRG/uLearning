package cat.uab.ulearning.model.services.interfaces;

import java.util.List;

import cat.uab.ulearning.model.entities.Quiz;

public interface QuizService extends GenericService<Quiz> {
	
	public List<Quiz> getQuizzesBySubject(int subjectCode, String userRole);

	public List<Quiz> getQuizzesFromAllUserSubjects(int userNiu, String userRole);
	
	public void updateQuizPublication(int idQuiz, boolean publish);
	
	public byte[] getQuizSubEntityImage(String entityName, int entityId);

}
